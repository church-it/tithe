package org.churchit.dizimo.data.semantics;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class InputTypeEnumTest {

    @Test
    public void getInstanceTest() {
        final int chequeId = InputTypeEnum.Cheque.getId();
        assertEquals(InputTypeEnum.getInstance(chequeId), InputTypeEnum.Cheque);

        final InputTypeEnum[] values = InputTypeEnum.values();
        final int anyInvalidId = values.length + 1;
        assertNull(InputTypeEnum.getInstance(anyInvalidId));
    }
}