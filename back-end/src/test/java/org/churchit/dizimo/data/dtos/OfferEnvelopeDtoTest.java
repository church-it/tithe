package org.churchit.dizimo.data.dtos;

import org.churchit.dizimo.data.semantics.InputTypeEnum;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNull;

public class OfferEnvelopeDtoTest {

    @Test
    void constructorTest() {
        assertDoesNotThrow(OfferEnvelopeDtoTest::__generateFooData);
        assertNull(__generateFooData().assertInvalidFields());
    }

    public static TransactionDataDto __generateFooData() {
        final OfferEnvelopeDto[] testItems = new OfferEnvelopeDto[] {
                getJohnDoe("Jeremiah Jasik"), getJohnDoe("Mattie Mclemore"),
                getJohnDoe("Giuseppe Galeana"), getJohnDoe("Ruthe Rohrer"),
                getJohnDoe("Rodger Rumph"), getJohnDoe("Gennie Grider"),
                getJohnDoe("Tracey Topham"), getJohnDoe("Darcie Dull"),
                getJohnDoe("Tracey Topham"),
                getJohnDoe("Raymond Replogle"), getJohnDoe("Pok Podesta")
        };

        return newFooIncomeTransactionDto(testItems);
    }

    private static TransactionDataDto newFooIncomeTransactionDto(OfferEnvelopeDto[] testItems) {
        return new TransactionDataDto()
                .setSignatories(new SignatoryDto[]{
                        new SignatoryDto().setFullName("Eduardo Aguiar Pacheco").setId("549.87 69"),
                        new SignatoryDto().setFullName("Marcos Valério Cristóvão de Souza")
                                .setId("000-1001 40 0")
                })
                .setEnvelopeDtos(testItems)
                .setDate(new Date())
                .setHashValue("foo-bar");
    }

    private static OfferEnvelopeDto getJohnDoe(String fullName) {
        Random random = new Random();
        OfferEnvelopeDto offerEnvelopeDto = new OfferEnvelopeDto();

        switch (random.nextInt(4)) {
            case 0:
                offerEnvelopeDto
                        .setInputTypeId(InputTypeEnum.Cheque.getId())
                        .setChequeNumber("# " + random.nextInt(1000000));
                break;
            case 1:
                offerEnvelopeDto.setInputTypeId(InputTypeEnum.Receipt.getId());
                break;
            default:
                offerEnvelopeDto.setInputTypeId(InputTypeEnum.Money.getId());
                break;
        }

        return offerEnvelopeDto
                .setFullName(fullName)
                .setComment(random.nextBoolean() ? "comment" : "")
                .setTitheAmount(1000.f+(random.nextInt(1000)))
                .setOffers(new OfferVoDto[]{new OfferVoDto().setAmount(300.f).setDestination("Oferta Igreja")});
    }

}