package org.churchit.dizimo.repository;

import org.churchit.dizimo.data.dtos.OfferEnvelopeDtoTest;
import org.churchit.dizimo.data.dtos.TransactionDataDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IncomeTransactionDataRepositoryTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void putIncomeTransaction() {
        final var dataRepository = new IncomeTransactionDataRepository();
        final Random random = new Random();

        final var dto1 = OfferEnvelopeDtoTest.__generateFooData();
        dataRepository.putIncomeTransaction(dto1);

        final String hashValue1 = dto1.getHashValue();
        final String hashValue2 = IncomeTransactionDataRepository.appendIterationToHash(hashValue1, 1);
        final String hashValue3 = IncomeTransactionDataRepository.appendIterationToHash(hashValue1, 2);

        final Date date2 = new Date(random.nextLong());
        final TransactionDataDto dto2 = dto1.clone().setDate(date2);
        dataRepository.putIncomeTransaction(dto2);

        final Date date3 = new Date(random.nextLong());
        final TransactionDataDto dto3 = dto1.clone().setDate(date3);
        dataRepository.putIncomeTransaction(dto3);

        assertEquals(dto3, dataRepository.getByHashValue(hashValue1));
        assertEquals(dto2, dataRepository.getByHashValue(hashValue2));
        assertEquals(dto1, dataRepository.getByHashValue(hashValue3));
    }

}