package org.churchit.dizimo.reports;

import org.churchit.dizimo.TestUtils;
import org.churchit.dizimo.data.dtos.OfferEnvelopeDtoTest;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;


class TransactionRevenueResumeReportTest {
    @Test
    void buildReportBodySection () {
        final String currentClassNamePath = TestUtils.getClassNameAsPath(TransactionRevenueResumeReportTest.class);

        assertDoesNotThrow(() -> new TransactionRevenueResumeReport(
                new File(currentClassNamePath + "_test.pdf"),
                OfferEnvelopeDtoTest.__generateFooData())
                .buildPdf()
        );
    }
}