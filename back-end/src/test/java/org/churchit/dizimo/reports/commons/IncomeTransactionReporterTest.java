package org.churchit.dizimo.reports.commons;

import org.churchit.dizimo.data.dtos.OfferEnvelopeDto;
import org.churchit.dizimo.data.dtos.OfferVoDto;
import org.churchit.dizimo.data.dtos.SignatoryDto;
import org.churchit.dizimo.data.dtos.TransactionDataDto;
import org.churchit.dizimo.data.semantics.InputTypeEnum;
import org.churchit.dizimo.reports.TransactionRevenueResumeReport;
import org.churchit.dizimo.reports.TransactionTitheRevenueReport;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class IncomeTransactionReporterTest {

    private static final File file = new File("test.pdf");
    private static final TransactionDataDto incomeTransactionHeader = new TransactionDataDto()
            .setHashValue("test")
            .setDate(new Date())
            .setSignatories(new SignatoryDto[]{
                    new SignatoryDto().setId("000").setFullName("person1"),
                    new SignatoryDto().setId("001").setFullName("person2")
            });

    private static final Float LOOSE_VALUE = 120.f;
    private static final OfferVoDto[] looseOffer = new OfferVoDto[]{new OfferVoDto().setAmount(LOOSE_VALUE)
                    .setDestination("-")};

    @BeforeEach
    void setUp() {
        if (file.exists()) {
            if (!file.delete()) {
                throw new Error();
            }
        }
    }

    @AfterEach
    void tearDown() {
        file.delete();
    }

    @Test
    void buildPdf() throws FileNotFoundException {
        acceptsSingleItems();
        checkImmutabilityOnZeroTithes();
    }

    private void checkImmutabilityOnZeroTithes() throws FileNotFoundException {
        final OfferEnvelopeDto anon1 = new OfferEnvelopeDto()
                .setChequeNumber("")
                .setInputTypeId(InputTypeEnum.Money.getId())
                .setFullName("person3")
                .setComment("")
                .setTitheAmount(0.f)
                .setOffers(looseOffer);

        final TransactionDataDto transactionDataDto = incomeTransactionHeader
                .setEnvelopeDtos(new OfferEnvelopeDto[]{anon1});

        new TransactionTitheRevenueReport(file, transactionDataDto)
                .buildPdf();

        final String message = "incomeList changed after running tithe-report.";

        assertEquals(1, transactionDataDto.getEnvelopeDtos().length, message);
        assertEquals(1, transactionDataDto.getEnvelopeDtos()[0].getOffers().length, message);
        assertEquals(LOOSE_VALUE, transactionDataDto.getEnvelopeDtos()[0].getOffers()[0].getAmount(), message);
    }

    private void acceptsSingleItems() {
        acceptsSingleLooseOffer();
        acceptsSingleCheque();
        acceptsSingleReceipt();
    }

    private void acceptsSingleLooseOffer() {
        final OfferEnvelopeDto anon1 = new OfferEnvelopeDto()
                .setChequeNumber("")
                .setInputTypeId(InputTypeEnum.Money.getId())
                .setFullName("person3")
                .setComment("")
                .setTitheAmount(0.f)
                .setOffers(looseOffer);

        assertDoesNotThrow(() -> {
            new TransactionRevenueResumeReport(
                    file,
                    incomeTransactionHeader
                            .setEnvelopeDtos(new OfferEnvelopeDto[]{anon1}))
                    .buildPdf();
        });

        assertDoesNotThrow(() -> {
            new TransactionTitheRevenueReport(file, incomeTransactionHeader
                    .setEnvelopeDtos(new OfferEnvelopeDto[]{anon1}))
                    .buildPdf();
        });
    }

    private void acceptsSingleCheque() {
        final OfferEnvelopeDto cheque1 = new OfferEnvelopeDto()
                .setInputTypeId(InputTypeEnum.Cheque.getId())
                .setChequeNumber("ok")
                .setFullName("person11")
                .setComment("")
                .setOffers(new OfferVoDto[0])
                .setTitheAmount(1200.f);

        assertDoesNotThrow(() -> {
            new TransactionRevenueResumeReport(file, incomeTransactionHeader
                    .setEnvelopeDtos(new OfferEnvelopeDto[]{cheque1}))
                    .buildPdf();
        });

        assertDoesNotThrow(() -> {
            new TransactionTitheRevenueReport(file, incomeTransactionHeader
                    .setEnvelopeDtos(new OfferEnvelopeDto[]{cheque1}))
                    .buildPdf();
        });
    }

    private void acceptsSingleReceipt() {
        OfferEnvelopeDto receipt1 = new OfferEnvelopeDto()
                .setInputTypeId(InputTypeEnum.Receipt.getId())
                .setChequeNumber("")
                .setFullName("Person22")
                .setComment("")
                .setOffers(new OfferVoDto[0])
                .setTitheAmount(950.f);

        assertDoesNotThrow(() -> {
            new TransactionRevenueResumeReport(file, incomeTransactionHeader
                    .setEnvelopeDtos(new OfferEnvelopeDto[]{receipt1}))
                    .buildPdf();
        });

        assertDoesNotThrow(() -> {
            new TransactionTitheRevenueReport(file, incomeTransactionHeader
                    .setEnvelopeDtos(new OfferEnvelopeDto[]{receipt1}))
                    .buildPdf();
        });
    }
}