package org.churchit.dizimo.reports;


import org.churchit.dizimo.TestUtils;
import org.churchit.dizimo.data.dtos.OfferEnvelopeDtoTest;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class TransactionTitheRevenueReportTest {

    @Test
    void buildReportBodySection() {
        final String currentClassNamePath = TestUtils.getClassNameAsPath(TransactionTitheRevenueReportTest.class);

        assertDoesNotThrow(() -> new TransactionTitheRevenueReport(
                new File(currentClassNamePath + "_test.pdf"),
                OfferEnvelopeDtoTest.__generateFooData()
                ).setAddSignatoriesSection(false).buildPdf()
        );
    }

}