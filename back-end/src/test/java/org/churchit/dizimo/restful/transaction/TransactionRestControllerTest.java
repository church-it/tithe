package org.churchit.dizimo.restful.transaction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

class TransactionRestControllerTest {

    @Test
    void getHashAndIterationObject() throws JsonProcessingException {
        final Object teste = TransactionRestController.getHashAndIterationObject(123, "teste");
        ObjectMapper mapper = new ObjectMapper();
        Assert.assertEquals("{\"hashValue\":\"teste\",\"iteration\":123}",
                mapper.writer().writeValueAsString(teste));
    }
}