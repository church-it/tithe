package org.churchit.dizimo;

public class TestUtils {
    public static String getClassNameAsPath(Class<?> clazz) {
        return "src/test/java/" + clazz.getName().replaceAll("\\.", "/");
    }
}
