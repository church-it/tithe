package org.churchit.pdfutils;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.draw.ILineDrawer;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TabAlignment;

import java.io.FileNotFoundException;
import java.util.Arrays;

import static org.churchit.pdfutils.ITextUtils.getDocumentWidth;

public class CenterText extends Paragraph {

    private final Text element;
    private final ILineDrawer line;
    private float width;

    public CenterText(Float width, String text) {
        this(width, new Text(text), null);
    }

    public CenterText(Document doc, String text, ILineDrawer line) {
        this(doc, new Text(text), line);
    }

    public CenterText(Document doc, Text text, ILineDrawer line) {
        this(getDocumentWidth(doc), text, line);
    }
    public CenterText(Float width, Text text, ILineDrawer line) {
        super();

        this.width = width;
        this.element = text;
        this.line = line;

        setTableStops();
        doLine();
    }

    private void setTableStops() {
        float center = this.width / 2;
        float end = this.width;

        this.addTabStops(Arrays.asList(
                new TabStop(center, TabAlignment.CENTER, line),
                new TabStop(end, TabAlignment.LEFT, line)
        ));
    }

    private void doLine() {
        this.add(new Tab());
        this.add(element);
        this.add(new Tab());
    }

    public static void main(String[] args) {
        PdfDocument pdf = null;

        try {
            pdf = new PdfDocument(new PdfWriter("./out/result.pdf"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Document document = new Document(pdf, PageSize.A4);
        CenterText p = new CenterText(document, "no meio", new CenteredLine());
        document.add(p);
        document.close();
    }
}
