package org.churchit.pdfutils;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.Document;

public final class ITextUtils {
    private ITextUtils() {}

    public static float getDocumentWidth(Document doc) {
        float pageSizeW = doc.getPdfDocument().getDefaultPageSize().getWidth();
        return pageSizeW - doc.getLeftMargin() - doc.getRightMargin();
    }

    public static float getPdfDocumentRawWidth(PdfDocument pdf) {
        return pdf.getDefaultPageSize().getWidth();
    }
}
