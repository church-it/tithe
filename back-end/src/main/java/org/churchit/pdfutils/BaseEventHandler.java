package org.churchit.pdfutils;

import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Canvas;

public abstract class BaseEventHandler implements IEventHandler {
    protected float marginY = 16f;
    protected float marginX = 40f;
    protected int pageNumber;
    protected Rectangle pageSize;

    private PdfCanvas pdfCanvas;

    @Override
    public void handleEvent(Event event) {
        PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
        PdfDocument pdf = docEvent.getDocument();
        PdfPage page = docEvent.getPage();
        this.pageSize = page.getPageSize();
        this.pdfCanvas = new PdfCanvas(page.getLastContentStream(), page.getResources(), pdf);
        Canvas canvas = new Canvas(pdfCanvas, pdf, pageSize);

        this.pageNumber = pdf.getPageNumber(page);

        this.handleEventCanvas(canvas);
        pdfCanvas.release();
    }

    protected abstract void handleEventCanvas(Canvas canvas);

    protected void addXObjectToPdf(PdfFormXObject placeholder, float x, float y) {
        pdfCanvas.addXObject(placeholder, x, y);
    }

    public BaseEventHandler setMarginY(float marginY) {
        this.marginY = marginY;
        return this;
    }

    public BaseEventHandler setMarginX(float marginX) {
        this.marginX = marginX;
        return this;
    }
}
