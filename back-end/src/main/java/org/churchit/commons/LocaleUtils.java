package org.churchit.commons;

import java.util.Locale;

public final class LocaleUtils {
    private LocaleUtils() {}

    public static final Locale LOCALE_BRAZIL = new Locale("pt", "BR");
    public static final String OPEN_QUOTE_MARK = "“";
    public static final String CLOSE_QUOTE_MARK = "”";
}
