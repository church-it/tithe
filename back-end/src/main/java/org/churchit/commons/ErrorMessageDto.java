package org.churchit.commons;

public class ErrorMessageDto {
    private final String message;

    public ErrorMessageDto(String responseMessage) {
        this.message = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getIsError() {
        return true;
    }
}
