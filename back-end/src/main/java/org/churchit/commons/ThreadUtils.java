package org.churchit.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ThreadUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadUtils.class);
    public static final int CALLING_METHOD_INDEX = 2;

    public static String getCurrentMethodName() {
        return getStackMethodName(CALLING_METHOD_INDEX +1);
    }

    public static String getStackMethodName(final int depth) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace.length > depth) {
            return stackTrace[depth].getMethodName();
        } else {
            LOGGER.error("unable to get method name; depth [" + depth + "]" +
                    " out of boundary [" + stackTrace.length + "]");
            return "<unknown>";
        }
    }

    private ThreadUtils() { }
}
