package org.churchit.dizimo.repository;

import org.churchit.dizimo.data.dtos.TransactionDataDto;
import org.churchit.dizimo.restful.transaction.utilities.LoggingUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Set;
import java.util.stream.Stream;


@Repository
public class IncomeTransactionDataRepository {

    private static HashMap<String, TransactionDataDto> table = new HashMap<>();
    private static final String ITERATION_DELIMITER = "##";
    private static final LoggingUtils LOGGING_UTILS = new LoggingUtils(IncomeTransactionDataRepository.class);


    public TransactionDataDto getByHashValue(String hashValue) {
        return table.get(hashValue);
    }

    /**
     * Adds a new Income Transaction to database.
     *
     * @param transactionDto transaction package to be added.
     * @return iteration number of this hash-key.
     * Default value is one. Returning one mean that this hash is new to the database,
     * if another object had this hash, it will be marked as an iteration and receive an iteration number.
     * When pulling an object, the most recent version will be sent unless an iteration number is provided.
     * Higher the iteration number, older is the data.
     */
    public int putIncomeTransaction(TransactionDataDto transactionDto) {
        final String hashValue = transactionDto.getHashValue();
        final Stream<String> filteredStream = table.keySet().stream()
                .filter(k -> k.startsWith(hashValue));

        int count = shiftAllIterations(filteredStream);
        table.put(hashValue, transactionDto);

        LOGGING_UTILS.logInformation("performed PUT",
                String.format("#%d: %s", count, transactionDto.getDate().toString()));

        return count;
    }

    private int shiftAllIterations(Stream<String> filteredStream) {
        HashMap<String, TransactionDataDto> aux = new HashMap<>();

        filteredStream.forEach(k -> {
            final TransactionDataDto value = table.get(k);
            final int indexOf = k.indexOf(ITERATION_DELIMITER);
            final int iteration;
            final String rootString;

            if (indexOf == -1) {
                iteration = 0;
                rootString = k;
            } else {
                iteration = Integer.parseInt(k.substring(indexOf + ITERATION_DELIMITER.length()));
                rootString = k.substring(0, indexOf);
            }

            aux.put(appendIterationToHash(rootString, iteration + 1), value);
        });

        aux.forEach(table::put);
        return aux.size();
    }

    static String appendIterationToHash(String rootString, int iteration) {
        return rootString + ITERATION_DELIMITER + iteration;
    }

    public Set<String> getListOfKeys() {
        return table.keySet();
    }
}
