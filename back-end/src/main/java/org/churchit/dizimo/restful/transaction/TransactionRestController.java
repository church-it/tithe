package org.churchit.dizimo.restful.transaction;

import org.churchit.dizimo.data.dtos.TransactionDataDto;
import org.churchit.dizimo.reports.TransactionRevenueResumeReport;
import org.churchit.dizimo.reports.TransactionTitheRevenueReport;
import org.churchit.dizimo.repository.IncomeTransactionDataRepository;
import org.churchit.dizimo.restful.transaction.utilities.LoggingUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Set;

import static org.churchit.dizimo.restful.transaction.utilities.TransactionControllerUtils.*;

@CrossOrigin
@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionRestController {

    private static final LoggingUtils LOGGING_UTILS = new LoggingUtils(TransactionRestController.class);

    private final IncomeTransactionDataRepository repository;

    public TransactionRestController(IncomeTransactionDataRepository repository) {
        this.repository = repository;
    }

    @GetMapping(path = "/")
    public Set<String> root() {
        LOGGING_UTILS.logReceivedRequestGet();
        return status();
    }

    @GetMapping(path = "/status")
    public Set<String> status() {
        return repository.getListOfKeys();
    }

    @PostMapping(path = "/push")
    public ResponseEntity<?> inputTransaction (@RequestBody TransactionDataDto transactionDataDto) {
        LOGGING_UTILS.logReceivedRequestPost(transactionDataDto);

        final String invalidMessage = transactionDataDto.assertInvalidFields();
        if (invalidMessage != null) {
            return badRequestResponse(invalidMessage);
        }

        final int iteration = repository.putIncomeTransaction(transactionDataDto);
        final String hashValue = transactionDataDto.getHashValue();
        final HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(getHashAndIterationObject(iteration, hashValue),
                httpHeaders, HttpStatus.ACCEPTED);
    }

    static Object getHashAndIterationObject(int iteration, String hashValue) {
        return new Object() {
            public int getIteration() {return iteration;}
            public String getHashValue() {return hashValue;}
        };
    }

    @GetMapping(path = "/tithe-report")
    public @ResponseBody ResponseEntity<?> titheIncomeTransactionReportPdf(@RequestParam String hashValue)
            throws IOException {
        LOGGING_UTILS.logReceivedRequestGet(hashValue);
        TransactionDataDto transactionDataDto = repository.getByHashValue(hashValue);

        final ResponseEntity<?> validationErrors = validateRequestHashValue(transactionDataDto);
        if (validationErrors != null) {
            return validationErrors;
        }

        final String reportFileName = TransactionTitheRevenueReport.REPORT_FILE_PREFIX;
        final byte[] pdfBytes = getTitheReportByteArray(transactionDataDto, reportFileName);

        return loadReportAsResponse(pdfBytes, reportFileName);
    }

    @GetMapping(path = "/revenue-report")
    public @ResponseBody ResponseEntity<?> revenueSummaryTransactionReportPdf(@RequestParam String hashValue)
            throws IOException {
        LOGGING_UTILS.logReceivedRequestGet(hashValue);
        TransactionDataDto transactionDataDto = repository.getByHashValue(hashValue);

        final ResponseEntity<?> validationErrors = validateRequestHashValue(transactionDataDto);
        if (validationErrors != null) {
            return validationErrors;
        }

        final String reportFileName = TransactionRevenueResumeReport.REPORT_FILE_PREFIX;
        final byte[] pdfBytes = getRevenueReportByteArray(transactionDataDto, reportFileName);
        return loadReportAsResponse(pdfBytes, reportFileName);
    }

    @GetMapping(path = "/pull")
    public @ResponseBody ResponseEntity<?> retrieveTransactionByHash(@RequestParam String hashValue) {
        LOGGING_UTILS.logReceivedRequestGet(hashValue);
        TransactionDataDto transactionDataDto = repository.getByHashValue(hashValue);

        final ResponseEntity<?> validationErrors = validateRequestHashValue(transactionDataDto);
        if (validationErrors != null) {
            return validationErrors;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(transactionDataDto, headers, HttpStatus.OK);
    }
}