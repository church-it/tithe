package org.churchit.dizimo.restful.transaction.utilities;

import org.churchit.commons.ErrorMessageDto;
import org.churchit.commons.ThreadUtils;
import org.churchit.dizimo.data.commons.IValidateDto;
import org.churchit.dizimo.data.dtos.TransactionDataDto;
import org.churchit.dizimo.reports.TransactionRevenueResumeReport;
import org.churchit.dizimo.reports.TransactionTitheRevenueReport;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import static org.churchit.commons.ThreadUtils.CALLING_METHOD_INDEX;
import static org.churchit.dizimo.reports.commons.ReportUtils.PDF_SUFFIX;
import static org.churchit.dizimo.reports.commons.ReportUtils.TEMP_FOLDER;

public final class TransactionControllerUtils {
    private static final String INCOME_TRANSACTION_NOT_FOUND = "could not find income-transaction";
    private static final LoggingUtils LOGGING_UTILS = new LoggingUtils(TransactionControllerUtils.class);

    private TransactionControllerUtils() {
    }

    public static ResponseEntity<byte[]> loadReportAsResponse(byte[] pdfBytes, String reportFileName) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData(reportFileName, reportFileName +PDF_SUFFIX);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return new ResponseEntity<>(pdfBytes, headers, HttpStatus.OK);
    }

    public static ResponseEntity<?> validateRequestHashValue(IValidateDto incomeTransactionDto) {
        if (incomeTransactionDto == null) {
            return badRequestResponse(INCOME_TRANSACTION_NOT_FOUND);
        }

        final String invalidFieldsMessage = incomeTransactionDto.assertInvalidFields();
        if (invalidFieldsMessage != null) {
            return badRequestResponse(invalidFieldsMessage);
        }

        return null;
    }

    public static ResponseEntity<?> badRequestResponse(String responseMessage) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final ResponseEntity<ErrorMessageDto> response = new ResponseEntity<>(new ErrorMessageDto(responseMessage), headers,
                HttpStatus.BAD_REQUEST);

        LOGGING_UTILS.logRespondingFailure(response, ThreadUtils.getStackMethodName(CALLING_METHOD_INDEX+1));
        return response;
    }

    public static byte[] getRevenueReportByteArray(TransactionDataDto transactionDataDto, String reportFileName) throws IOException {
        final String filePrefix = String.format("%s.%s.", reportFileName,
                new Date().toString());

        final File tempFile = File.createTempFile(filePrefix, PDF_SUFFIX, TEMP_FOLDER);

        final File pdfFile = new TransactionRevenueResumeReport(tempFile, transactionDataDto)
                .buildPdf();

        final byte[] bytes = Files.readAllBytes(Paths.get(pdfFile.toURI()));
        pdfFile.deleteOnExit();
        return bytes;
    }

    public static byte[] getTitheReportByteArray(TransactionDataDto transactionDataDto, String reportFileName) throws IOException {
        final String filePrefix = String.format("%s.%s.", reportFileName, new Date().toString());
        final File tempFile = File.createTempFile(filePrefix, PDF_SUFFIX, TEMP_FOLDER);

        final File pdfFile = new TransactionTitheRevenueReport(tempFile, transactionDataDto)
                .setAddSignatoriesSection(false)
                .buildPdf();

        final byte[] bytes = Files.readAllBytes(Paths.get(pdfFile.toURI()));
        pdfFile.deleteOnExit();
        return bytes;
    }
}
