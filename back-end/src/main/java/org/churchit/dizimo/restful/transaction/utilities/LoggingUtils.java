package org.churchit.dizimo.restful.transaction.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.churchit.commons.ThreadUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import static org.churchit.commons.ThreadUtils.CALLING_METHOD_INDEX;

public final class LoggingUtils {
    private static final ObjectWriter prettyPrinter = new ObjectMapper().writerWithDefaultPrettyPrinter();

    private final Logger logger;

    public LoggingUtils(Class<?> clazz) {
        this.logger = LoggerFactory.getLogger(clazz);
    }

    public void logReceivedRequestPost(Object... objects) {
        logReceivedRequest("POST", getCallerMethodName(), objects);
    }

    private String getCallerMethodName() {
        return ThreadUtils.getStackMethodName(CALLING_METHOD_INDEX + 2);
    }

    public void logReceivedRequestGet(Object... objects) {
        logReceivedRequest("GET", getCallerMethodName(), objects);
    }

    private void logReceivedRequest(String restType, String methodName, Object... payload) {
        StringBuilder sb = patternLogBuilder("received " + restType, methodName);

        if (payload.length > 0) {
            sb.append("\n> PAYLOAD");

            for (int i = 0; i < payload.length; i++) {
                try {
                    sb.append("\nobj_").append(i).append(": ");
                    sb.append(prettyPrinter.writeValueAsString(payload[i]));
                } catch (JsonProcessingException e) {
                    sb.append("\n> ERROR while writing json:");
                    sb.append(e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        logger.info(sb.toString());
    }

    private StringBuilder patternLogBuilder(String logTitle, String methodName) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n> ").append(logTitle).append(": ").append(methodName);
        return sb;
    }

    public void logInformation(String title, Object info) {
        StringBuilder sb = patternLogBuilder(title, getCallerMethodName());
        sb.append("\n> ").append(info);
        logger.info(sb.toString());
    }

    void logRespondingFailure(ResponseEntity<?> response, String methodName) {
        StringBuilder sb = patternLogBuilder("responding FAILURE", methodName);

        sb.append("\n> PAYLOAD\n");

        try {
            sb.append(prettyPrinter.writeValueAsString(response));
        } catch (JsonProcessingException e) {
            sb.append("\n> ERROR while writing json:");
            sb.append(e.getMessage());
            e.printStackTrace();
        }

        logger.warn(sb.toString());
    }
}
