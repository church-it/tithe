package org.churchit.dizimo.reports.commons;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.TabAlignment;
import org.churchit.pdfutils.ITextUtils;

import static org.churchit.dizimo.reports.commons.ReportUtils.CORPORATE_NAME_DISPLAY;
import static org.churchit.dizimo.reports.commons.ReportUtils.LOGO_PNG_URL;

class ReportStaticHeader extends Paragraph {

    private static final int LOGO_HEIGHT = 85;
    private static final int DEFAULT_HEADER_FONT_SIZE = 15;
    private final String keyReference;
    private final String reportName;
    private final Document document;

    ReportStaticHeader(String reportName, String keyReference, Document document) {
        super();
        this.keyReference = keyReference;
        this.reportName = reportName;
        this.document = document;

        init();
    }

    private void init() {
        final Text corporateName = new Text(CORPORATE_NAME_DISPLAY);
        final Text transactionKeyReference = new Text(keyReference);
        final Text reportTitle = new Text(reportName);
        reportTitle.setFont(ReportUtils.Fonts.defaultNormal());

        final Paragraph headerLeft = new Paragraph()
                .add(corporateName).add("\n")
                .add(reportTitle).add("\n")
                .add(transactionKeyReference).add("\n")
                .add("\n");

        final Image logoImage =
                new Image(ImageDataFactory.createPng(LOGO_PNG_URL))
                        .setHeight(LOGO_HEIGHT)
                        .setAutoScale(true);

        final Paragraph headerRight = new Paragraph()
                .add(logoImage);

        final float center = ITextUtils.getDocumentWidth(document);
        this.addTabStops(new TabStop(center, TabAlignment.RIGHT))
                .setFontSize(DEFAULT_HEADER_FONT_SIZE)
                .add(headerLeft)
                .add(new Tab())
                .add(headerRight);
    }

    public String getKeyReference() {
        return keyReference;
    }
}
