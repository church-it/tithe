package org.churchit.dizimo.reports.commons;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.element.BlockElement;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.IElement;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.UnitValue;
import org.churchit.dizimo.DizimoApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;

import static com.itextpdf.io.font.PdfEncodings.IDENTITY_H;
import static org.churchit.commons.LocaleUtils.LOCALE_BRAZIL;

public final class ReportUtils {
    private static Logger LOGGER = LoggerFactory.getLogger(ReportUtils.class);

    public static final String HEART_SYMBOL = "\u2665";
    public static final String UNREGISTERED_TRADE_MARK_SYMBOL = "\u2122";
    public static final String DIZIMO_SIGNATURE = "PDF Dízimo" + UNREGISTERED_TRADE_MARK_SYMBOL + ", oferta de "
            + HEART_SYMBOL + " por Church IT — Brasil";


    public static final String CORPORATE_ID_CAPTION_TEXT = "CNPJ";
    public static final String CORPORATE_NAME_DISPLAY = "PRIMEIRA IGREJA BATISTA EM SÃO CARLOS";
    public static final String CORPORATE_ID_VALUE = "46.704.516/0001-18";
    public static final Locale DEFAULT_LOCALE = LOCALE_BRAZIL;
    public static final NumberFormat CURRENCY_INSTANCE = NumberFormat.getCurrencyInstance(DEFAULT_LOCALE);

    public static URL LOGO_PNG_URL = DizimoApplication.class.getClassLoader()
            .getResource("static/logo-pibsc-preto_1612x1376.png");
    public static URL SYMBOLS_FONT_PROGRAM = DizimoApplication.class.getClassLoader()
            .getResource("fonts/DejaVuSerif.ttf");

    public static final File TEMP_FOLDER = new File("./temp");
    public static final String PDF_SUFFIX = ".pdf";

    private ReportUtils() {
    }

    static void consumeIfBlockElement(IElement element, Consumer<BlockElement> consumer) {
        if (element instanceof BlockElement) {
            BlockElement blockElement = (BlockElement) element;
            consumer.accept(blockElement);
        }
    }

    static java.util.List<Cell> tuplesToCells(Map<? extends Object, Double> reduction) {
        java.util.List<Cell> rows = new ArrayList<>();

        reduction.forEach((key, value) -> {
            rows.add(new Cell().add(new Paragraph(key.toString())));
            rows.add(new Cell().add(new Paragraph(CURRENCY_INSTANCE.format(value))));
        });

        rows.forEach(c->c.setWidth(UnitValue.createPercentValue(.5f)));
        return rows;
    }

    public static abstract class Colors {
        public static final DeviceRgb SCARLET = new DeviceRgb(255, 36, 0);
    }

    public static abstract class Fonts {
        public static PdfFont defaultNormal() {
            return getFontOrDefault(StandardFonts.TIMES_ROMAN);
        }

        public static PdfFont defaultBold() {
            return getFontOrDefault(StandardFonts.TIMES_BOLD);
        }

        public static PdfFont defaultOblique() {
            return getFontOrDefault(StandardFonts.TIMES_ITALIC);
        }

        public static PdfFont defaultBoldOblique() {
            return getFontOrDefault(StandardFonts.TIMES_BOLDITALIC);
        }

        public static PdfFont dizimoSignature() {
            return getFontOrDefault(StandardFonts.COURIER);
        }

        public static PdfFont specialSymbols() {
            return getFontOrDefault(SYMBOLS_FONT_PROGRAM);
        }

        public static PdfFont getFontOrDefault(String fontName) {
            PdfFont deferredFont = null;

            try {
                // following is unlikely to fail; works as default font
                deferredFont = PdfFontFactory.createFont();
                deferredFont = PdfFontFactory.createFont(fontName);
            } catch (IOException e) {
                LOGGER.error("Unable to load default font: " + fontName, e);
            }

            return deferredFont;
        }

        public static PdfFont getFontOrDefault(URL resource) {
            PdfFont deferredFont = null;

            try {
                final byte[] bytes = resource.openStream().readAllBytes();

                // following is unlikely to fail; works as default font
                deferredFont = PdfFontFactory.createFont();
                deferredFont = PdfFontFactory.createFont(bytes, IDENTITY_H);
            } catch (IOException e) {
                LOGGER.error("Unable to load default font: " + resource.getFile(), e);
            }

            return deferredFont;
        }
    }
}
