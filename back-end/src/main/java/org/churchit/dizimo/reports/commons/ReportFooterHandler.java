package org.churchit.dizimo.reports.commons;

import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import org.churchit.pdfutils.BaseEventHandler;

public class ReportFooterHandler extends BaseEventHandler {

    private final String[] lines;
    private boolean skipFirstPage = true;
    private boolean displayDizimoSignature = true;

    public ReportFooterHandler(String ...lines) {
        this.lines = lines;
    }

    @Override
    public void handleEventCanvas(Canvas canvas) {
        final boolean doFooter = this.pageNumber != 1 || !skipFirstPage;
        final boolean doDizimoSign = this.pageNumber == 1 && displayDizimoSignature;

        if (!doFooter && !doDizimoSign) {
            return;
        }

        Paragraph p = new Paragraph();
        addFooterLines(p, doFooter);
        addDizimoSignatureLine(p, doDizimoSign);

        float x = this.marginX;
        float y = this.marginY;
        canvas.showTextAligned(p, x, y, TextAlignment.LEFT);
    }

    private void addDizimoSignatureLine(Paragraph p, boolean doDizimoSign) {
        if (!doDizimoSign) { return; }

        final String[] dizimoSignature = (ReportUtils.DIZIMO_SIGNATURE+"\n").split(ReportUtils.HEART_SYMBOL);

        assert dizimoSignature.length == 2: "BROKEN LOGIC: dizimoSignature doesn't split in three";

        p.add(
                new Text(dizimoSignature[0]).setFont(ReportUtils.Fonts.dizimoSignature())
        ).add(
                new Text(ReportUtils.HEART_SYMBOL).setFont(ReportUtils.Fonts.specialSymbols())
        ).add(
                new Text(dizimoSignature[1]).setFont(ReportUtils.Fonts.dizimoSignature())
        );
    }

    private void addFooterLines(Paragraph p, boolean doFooter) {
        if (!doFooter) { return; }

        for (String line : this.lines) {
            p.add(line + "\n");
        }
    }
}
