package org.churchit.dizimo.reports.commons;

public class MemoizationKey implements Comparable {
    private final String key;

    public MemoizationKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof MemoizationKey)) {
            return -1;
        }

        final MemoizationKey memo1 = (MemoizationKey) o;
        return this.key.compareTo(memo1.key);
    }
}