package org.churchit.dizimo.reports.commons;

import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import org.churchit.pdfutils.BaseEventHandler;

public class PageCountingHandler extends BaseEventHandler {
    private static final String PAGINATION_REASON_STR = " de ";
    private static final String PAGINATION_PREFIX = "Pág. ";

    private float finalMarginCorrectionSize = 4.5f;
    private PdfFormXObject placeholder;

    /**
     * Writes the default document footer.
     * By default, total page count display is expected but will only occur after a call
     * to {@link PageCountingHandler#writeTotalPagesCount} inner method.
     */
    public PageCountingHandler() {
        placeholder = new PdfFormXObject(new Rectangle(0, 0, 20, 20));
    }

    @Override
    public void handleEventCanvas(Canvas canvas) {
        Paragraph p = new Paragraph()
                .add(PAGINATION_PREFIX + this.pageNumber + PAGINATION_REASON_STR);

        float x = this.pageSize.getWidth() - marginX;
        float y = marginY;
        canvas.showTextAligned(p, x, y, TextAlignment.RIGHT);

        this.addXObjectToPdf(placeholder, x, y);
    }

    /**
     * Call this method after adding the handler;
     * It will print the total page count after the current page number.
     * @param pdf the pdf document to print at can be got by {@link Document#getPdfDocument()}
     */
    public void writeTotalPagesCount(PdfDocument pdf) {
        Canvas canvas = new Canvas(placeholder, pdf);
        canvas.showTextAligned(String.valueOf(pdf.getNumberOfPages()),
                finalMarginCorrectionSize, 0, TextAlignment.LEFT);
    }

    public PageCountingHandler setFinalMarginCorrectionSize(float finalMarginCorrectionSize) {
        this.finalMarginCorrectionSize = finalMarginCorrectionSize;
        return this;
    }
}
