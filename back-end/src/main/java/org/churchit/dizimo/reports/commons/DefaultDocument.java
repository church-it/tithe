package org.churchit.dizimo.reports.commons;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;

public class DefaultDocument extends Document {

    static final int DEFAULT_FONT_SIZE = 14;
    static final int DEFAULT_FONT_SMALL_SIZE = 10;
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDocument.class);

    public DefaultDocument(File file) throws FileNotFoundException {
        super(new PdfDocument(new PdfWriter(file)));
        setDefaults();
    }

    private void setDefaults() {
        this.setFont(ReportUtils.Fonts.defaultNormal());
        this.setFontSize(DEFAULT_FONT_SIZE);
    }

}
