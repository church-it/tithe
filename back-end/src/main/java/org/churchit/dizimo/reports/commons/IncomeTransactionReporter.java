package org.churchit.dizimo.reports.commons;

import com.itextpdf.kernel.colors.DeviceGray;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.ListNumberingType;
import com.itextpdf.layout.property.UnitValue;
import org.churchit.dizimo.data.dtos.OfferEnvelopeDto;
import org.churchit.dizimo.data.dtos.SignatoryDto;
import org.churchit.dizimo.data.dtos.TransactionDataDto;
import org.churchit.dizimo.data.semantics.InputTypeEnum;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static org.churchit.dizimo.reports.commons.DefaultDocument.DEFAULT_FONT_SMALL_SIZE;
import static org.churchit.dizimo.reports.commons.ReportUtils.*;

public abstract class IncomeTransactionReporter {
    private static final DateFormat keyReferenceFormatter = DateFormat.getDateInstance(DateFormat.FULL, DEFAULT_LOCALE);
    private static final DeviceGray LIGHT_GRAY = new DeviceGray(.8f);
    private static final String SIGNATURES_SECTION_LABEL = "Assinaturas";
    private static final float SIGNATURE_SQUARE_HEIGHT = 70;
    private static final String TOTAL_SUM_LABEL = "TOTAL";
    private static final String NOTICE_RECEIPTS_WARNING = "* Atenção! Recibos não inclusos.";

    public static final MemoizationKey TITHE_RECEIPT_SUM_MEMO = new MemoizationKey("TITHE_RECEIPT_SUM");
    public static final MemoizationKey TOTAL_TITHE_MEMO = new MemoizationKey("TOTAL_TITHE_MEMO");

    private final File file;
    final protected TransactionDataDto transactionDataDto;

    private final String reportName;
    private final String reportSmallName;
    private final String keyReference;

    private boolean addDefaultHeader = true;
    private boolean addDefaultFooter = true;
    private boolean addSignatoriesSection = true;

    final protected Map<MemoizationKey, Double> scoreValuesMap = new HashMap<>();

    protected IncomeTransactionReporter(File file, TransactionDataDto transactionDataDto, String reportName,
                                        String reportSmallName) throws IllegalArgumentException {

        this.file = Objects.requireNonNull(file, "file required" );
        this.reportName = Objects.requireNonNull(reportName, "reportName inner parameter required");
        this.transactionDataDto = Objects.requireNonNull(transactionDataDto,
                "incomeTransactionDto required");
        this.reportSmallName = Objects.requireNonNull(reportSmallName,
                "reportSmallName inner parameter required");

        final String invalidFields = transactionDataDto.assertInvalidFields();
        if (invalidFields != null) {
            throw new IllegalArgumentException(invalidFields);
        }

        this.keyReference = keyReferenceFormatter.format(transactionDataDto.getDate());
    }

    /**
     * Builds the income transaction report in PDF format using instance properties into file
     * @return same file from instance
     * @throws FileNotFoundException thrown when unable to associate file to PDF writer.
     */
    public final File buildPdf() throws FileNotFoundException {
        final Document doc = new DefaultDocument(this.file);
        final PdfDocument pdfDocument = doc.getPdfDocument();

        final ReportStaticHeader header = new ReportStaticHeader(this.reportName, keyReference, doc);

        if (this.addDefaultHeader) {
            doc.add(header);
        }

        var rootList = new com.itextpdf.layout.element.List(ListNumberingType.DECIMAL);
        List<ListItem> listItems = this.buildReportBodySection();

        if (this.addSignatoriesSection) {
            final ListItem e = buildDefaultSignaturesSection(transactionDataDto.getSignatories());
            listItems = new ArrayList<>(listItems);
            listItems.add(e);
        }

        applyBodyListStyling(listItems);
        listItems.forEach(rootList::add);
        doc.add(rootList);

        if (this.addDefaultFooter) {
            buildFooter(pdfDocument);
        }

        doc.close();

        return this.file;
    }

    protected ListItem buildTitheTypesSummarizeSection(OfferEnvelopeDto[] incomeList, String title) {
        Map<InputTypeEnum, Double> reduction = collectTypesReduction(incomeList);
        Double receiptAmount = getTitheReceiptAmount(reduction);

        return buildTitheSummarizeSection(reduction, receiptAmount, title);
    }

    private Double getTitheReceiptAmount(Map<InputTypeEnum, Double> reduction) {
        final Double scored = scoreValuesMap.get(TITHE_RECEIPT_SUM_MEMO);
        if (scored != null) { return scored; }

        final Double sum = reduction.getOrDefault(InputTypeEnum.Receipt, 0.d);
        scoreValuesMap.put(TITHE_RECEIPT_SUM_MEMO, sum);

        return sum;
    }

    private static Map<InputTypeEnum, Double> collectTypesReduction(OfferEnvelopeDto[] records) {
        return Arrays.stream(records)
                .collect(
                        Collectors.groupingBy(
                                o -> InputTypeEnum.getInstance(o.getInputTypeId()),
                                Collectors.summingDouble(
                                        OfferEnvelopeDto::getTitheAmount)
                        ));
    }

    protected void applyBodyListStyling(java.util.List<ListItem> items) {
        items.forEach(IncomeTransactionReporter::setRootItemStyle);
        items.forEach(IncomeTransactionReporter::setChildItemsStyle);
        items.forEach(i->i.setMarginBottom(16));
    }

    protected static void applyDefaultTableHeaderStyling(Table header) {
        header.setFont(ReportUtils.Fonts.defaultBold());
        header.setBackgroundColor(LIGHT_GRAY);
    }

    private static ListItem buildDefaultSignaturesSection(SignatoryDto[] signatories) {
        float[] columnWidths = new float[]{.45f, .1f, .45f};
        Table table = new Table(UnitValue.createPercentArray(columnWidths));
        table.useAllAvailableWidth();

        // creates the whole table:
        // two signatories plus one gap
        int cellCount = (int) (3 * Math.ceil(signatories.length / 2.f));
        Arrays.asList(new Cell[cellCount]).forEach(c -> table.addCell(
                new Cell().setBorder(Border.NO_BORDER)
        ));

        populateSigningFields(signatories, table);

        ListItem listItem = new ListItem();
        listItem.add(new Paragraph(SIGNATURES_SECTION_LABEL)
                .setMarginBottom(16));
        listItem.add(table);
        return listItem;
    }

    private static void setRootItemStyle(ListItem item) {
        if (item.getChildren() == null || item.getChildren().size() == 0)
            return;

        IElement rootElement = item.getChildren().get(0);
        if (rootElement instanceof BlockElement) {
            BlockElement blockRoot = (BlockElement) rootElement;
            blockRoot.setFont(ReportUtils.Fonts.defaultBold());
        }
    }

    private static void setChildItemsStyle(ListItem item) {
        if (item.getChildren() == null || item.getChildren().size() < 2)
            return;

        java.util.List<IElement> children = item.getChildren().stream().skip(1).collect(Collectors.toList());
        children.forEach(element -> consumeIfBlockElement(element,
                (b) -> b.setMargins(0, 24, 0, 24)));
    }

    private static void populateSigningFields(SignatoryDto[] signatories, Table table) {
        for (int i = 0; i < signatories.length; ++i) {
            SignatoryDto signatoryDto = signatories[i];
            // col is either 0 or 2
            int column = (i % 2) * 2;
            // row step is 1 every 2: 0,0,1,1,2,2,...
            int row = (i / 2);

            Cell rectangle = new Cell()
                    .setBorder(new SolidBorder(1))
                    .setHeight(SIGNATURE_SQUARE_HEIGHT);

            Cell caption = new Cell()
                    .add(new Paragraph(
                            signatoryDto.getFullName() + "\n"
                                    + signatoryDto.getId() + "\n"))
                    .setHeight(UnitValue.createPercentValue(1.f))
                    .setBorder(Border.NO_BORDER);

            table.getCell(row, column).add(
                    new Table(1)
                            .setKeepTogether(true)
                            .useAllAvailableWidth()
                            .addCell(rectangle)
                            .addCell(caption))
                    .setPadding(0);
        }
    }

    private void buildFooter(PdfDocument pdfDocument) {
        PageCountingHandler pageCount = new PageCountingHandler();
        pdfDocument.addEventHandler(PdfDocumentEvent.END_PAGE, pageCount);

        final String companyFooterIdentity = String.format("%s – %s: %s",
                ReportUtils.CORPORATE_NAME_DISPLAY, ReportUtils.CORPORATE_ID_CAPTION_TEXT,
                ReportUtils.CORPORATE_ID_VALUE);

        final String reportFooterIdentity = String.format("%s: %s",
                reportSmallName, getKeyReference());

        pdfDocument.addEventHandler(PdfDocumentEvent.END_PAGE,
                new ReportFooterHandler(companyFooterIdentity, reportFooterIdentity));

        pageCount.writeTotalPagesCount(pdfDocument);
    }

    /**
     * Builds the report main section, after the Header and before the Footer.
     * @return List of items composing the body.
     */
    protected abstract java.util.List<ListItem> buildReportBodySection();

    protected String getKeyReference() {
        return keyReference;
    }

    public IncomeTransactionReporter setAddDefaultHeader(boolean addDefaultHeader) {
        this.addDefaultHeader = addDefaultHeader;
        return this;
    }

    public IncomeTransactionReporter setAddDefaultFooter(boolean addDefaultFooter) {
        this.addDefaultFooter = addDefaultFooter;
        return this;
    }

    public IncomeTransactionReporter setAddSignatoriesSection(boolean addSignatoriesSection) {
        this.addSignatoriesSection = addSignatoriesSection;
        return this;
    }

    protected ListItem buildTitheSummarizeSection(Map<?, Double> reduction, Double receiptAmount,
                                                         String sectionTitle) {
        Table table = new Table(equallyDistributedColumns(2));
        table.useAllAvailableWidth();

        tuplesToCells(reduction).forEach(table::addCell);

        // Receipts are not to be computed on total income.
        final Double totalIncomeButReceipts = getTotalTitheButReceipts(reduction, receiptAmount);
        table = someSummarizeSectionFooter(table, totalIncomeButReceipts, receiptAmount != 0);

        ListItem listItem = new ListItem();
        listItem.add(new Paragraph(sectionTitle));
        listItem.add(table);
        return listItem;
    }



    protected static Table someSummarizeSectionFooter(Table table, Double totalIncomeButReceipts,
                                                     boolean includesReceipts) {
        return someSummarizeSectionFooter(table, totalIncomeButReceipts, includesReceipts, 2);
    }

    protected static Table someSummarizeSectionFooter(Table table, Double totalIncomeButReceipts,
                                                     boolean includesReceipts, int colCount) {
        final Paragraph receiptWarningParagraph = new Paragraph(NOTICE_RECEIPTS_WARNING);

        final Cell left_totalCell = new Cell()
                .add(new Paragraph(TOTAL_SUM_LABEL));

        final Cell right_amountCell = new Cell()
                .add(new Paragraph(CURRENCY_INSTANCE.format(totalIncomeButReceipts)));

        if (includesReceipts) {
            left_totalCell.add(receiptWarningParagraph.setFontSize(DEFAULT_FONT_SMALL_SIZE));
        }

        if (colCount > 2) {
            table.addFooterCell(encapsulateIntoRow(left_totalCell, right_amountCell, colCount));
        } else {
            table.addFooterCell(left_totalCell).addFooterCell(right_amountCell);
        }

        styleFooter(table.getFooter());
        receiptWarningParagraph.setFont(ReportUtils.Fonts.defaultNormal());
        return table;
    }

    private static Cell encapsulateIntoRow(Cell left, Cell right, int colCount) {
        left.setBorder(Border.NO_BORDER)
                .setBorderRight(new SolidBorder(Border.SOLID));

        right.setBorder(Border.NO_BORDER)
                .setPaddingLeft(8);

        return new Cell(1, colCount).add(
                new Table(equallyDistributedColumns(2))
                        .addCell(left)
                        .addCell(right)
                        .useAllAvailableWidth()
        );
    }

    protected static UnitValue[] equallyDistributedColumns(int colCount) {
        float shareRate = 100.f / colCount;
        final float[] colSizes = new float[colCount];
        Arrays.fill(colSizes, shareRate);
        return UnitValue.createPercentArray(colSizes);
    }

    private Double getTotalTitheButReceipts(Map<?, Double> reduction, Double receiptsAmount) {
        final Double totalSum = getTotalTithe(reduction);

        return totalSum - receiptsAmount;
    }

    private Double getTotalTithe(Map<?, Double> reduction) {
        final Double scored = scoreValuesMap.get(TOTAL_TITHE_MEMO);

        if (scored != null) { return scored; }

        final Double sum = reduction.values().stream()
                .reduce(Double::sum)
                .orElse(0.d);

        scoreValuesMap.put(TOTAL_TITHE_MEMO, sum);
        return sum;
    }

    private static void styleFooter(Table footer) {
        footer.setFont(ReportUtils.Fonts.defaultBold());

        // TODO: this doesn't work
        // align only the "TOTAL" cell to the right
        footer.getCell(0, 0).getChildren().forEach(i -> consumeIfBlockElement(i,
                (b) -> b.setHorizontalAlignment(HorizontalAlignment.RIGHT)));
    }
}
