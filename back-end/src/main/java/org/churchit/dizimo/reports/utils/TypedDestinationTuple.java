package org.churchit.dizimo.reports.utils;

import org.churchit.dizimo.data.semantics.InputTypeEnum;

import java.util.Objects;

public class TypedDestinationTuple implements Comparable<TypedDestinationTuple> {

    private String destination;
    private InputTypeEnum inputTypeEnum;

    public String getDestination() {
        return destination;
    }

    public TypedDestinationTuple setDestination(String destination) {
        this.destination = Objects.requireNonNull(destination);
        return this;
    }

    public InputTypeEnum getInputTypeEnum() {
        return inputTypeEnum;
    }

    public TypedDestinationTuple setInputTypeEnum(InputTypeEnum inputTypeEnum) {
        this.inputTypeEnum = Objects.requireNonNull(inputTypeEnum);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedDestinationTuple that = (TypedDestinationTuple) o;
        return destination.equals(that.destination) &&
                inputTypeEnum == that.inputTypeEnum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(destination, inputTypeEnum);
    }

    @Override
    public int compareTo(TypedDestinationTuple o) {
        final int compA = destination.compareTo(o.destination);

        if (compA != 0) {
            return compA;
        }

        return inputTypeEnum.compareTo(o.inputTypeEnum);
    }
}
