package org.churchit.dizimo.reports;

import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.UnitValue;
import org.churchit.dizimo.data.dtos.OfferEnvelopeDto;
import org.churchit.dizimo.data.dtos.TransactionDataDto;
import org.churchit.dizimo.data.semantics.InputTypeEnum;
import org.churchit.dizimo.reports.commons.IncomeTransactionReporter;
import org.churchit.dizimo.reports.commons.ReportUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.churchit.commons.LocaleUtils.CLOSE_QUOTE_MARK;
import static org.churchit.commons.LocaleUtils.OPEN_QUOTE_MARK;
import static org.churchit.dizimo.reports.commons.ReportUtils.CURRENCY_INSTANCE;

public class TransactionTitheRevenueReport extends IncomeTransactionReporter {

    public static final String REPORT_FILE_PREFIX = "relacao-dizimos";

    private static final String SUMMARIZE_SECTION_TITLE = "Sumarização";
    private static final String TITHES_SECTION_LABEL = "Dízimos";

    private static final int SHOW_INDEX_PERIOD = 3;
    private static final String REPORT_NAME = "Relação de Dízimos";
    private static final String REPORT_SMALL_NAME = "DÍZIMOS";

    public TransactionTitheRevenueReport(File file, TransactionDataDto transactionDataDto) {
        super(file, transactionDataDto.clone(), REPORT_NAME, REPORT_SMALL_NAME);

        // removes empty tithes, they will not be considered.
        this.transactionDataDto.setEnvelopeDtos(
                Arrays.stream(transactionDataDto.getEnvelopeDtos())
                        .filter(i -> i.getTitheAmount() != 0)
                        .toArray(OfferEnvelopeDto[]::new));
    }

    @Override
    protected java.util.List<ListItem> buildReportBodySection() {
        ListItem titheListItem = buildTithesSection();
        ListItem summarizeItem = buildTitheTypesSummarizeSection(transactionDataDto.getEnvelopeDtos(), SUMMARIZE_SECTION_TITLE);
        return Arrays.asList(titheListItem, summarizeItem);
    }

    private ListItem buildTithesSection() {
        float[] columnWidths = {.05f, .4f, .275f, .275f};
        java.util.List<String> tableHeaders = Arrays.asList("","Nome","Detalhe","Valor");

        Table table = new Table(UnitValue.createPercentArray(columnWidths));
        table.useAllAvailableWidth();

        tableHeaders.forEach(table::addHeaderCell);
        applyDefaultTableHeaderStyling(table.getHeader());

        mapRecordListToTitheRow(transactionDataDto.getEnvelopeDtos())
                .forEach(table::addCell);

        ListItem listItem = new ListItem();
        listItem.add(new Paragraph(TITHES_SECTION_LABEL));
        listItem.add(table);
        return listItem;
    }

    private java.util.List<Cell> mapRecordListToTitheRow(OfferEnvelopeDto[] records) {
        java.util.List<Cell> rows = new ArrayList<>();

        for (int i = 0; i < records.length; i++) {
            OfferEnvelopeDto item = records[i];

                rows.add(new Cell().add(new Paragraph(
                        i % SHOW_INDEX_PERIOD == 0 || i == records.length-1
                                ? i + 1 + "."
                                : ""
                )));

            rows.addAll(mapItemToRow(item));
        }

        return rows;
    }

    private java.util.List<Cell> mapItemToRow(OfferEnvelopeDto item) {
        List<Text> details = getDetails(item);

        return Arrays.asList(
                new Cell().add(new Paragraph(item.getFullName())),
                new Cell().add(new Paragraph().addAll(details)),
                new Cell().add(new Paragraph(CURRENCY_INSTANCE.format(item.getTitheAmount())))
        );
    }

    private List<Text> getDetails(OfferEnvelopeDto item) {
        List<Text> details = new ArrayList<>();

        if (item.getInputTypeId() == InputTypeEnum.Receipt.getId()) {
            details.add(new Text("(somente recibo)\n"));
        } else if (item.getChequeNumber() != null)  {
            details.add(new Text("" +
                    "(cheque: " + item.getChequeNumber()+")\n"));
        }

        final String comment = item.getComment();
        if (comment != null && !comment.isBlank()) {
            details.add(new Text(OPEN_QUOTE_MARK + comment + CLOSE_QUOTE_MARK + "\n")
                    .setFont(ReportUtils.Fonts.defaultOblique()));
        }
        return details;
    }
}
