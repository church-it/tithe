package org.churchit.dizimo.reports;

import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.ListItem;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;
import org.churchit.dizimo.data.dtos.OfferEnvelopeDto;
import org.churchit.dizimo.data.dtos.OfferVoDto;
import org.churchit.dizimo.data.dtos.TransactionDataDto;
import org.churchit.dizimo.data.semantics.InputTypeEnum;
import org.churchit.dizimo.data.semantics.OfferConstants;
import org.churchit.dizimo.reports.commons.IncomeTransactionReporter;
import org.churchit.dizimo.reports.commons.MemoizationKey;
import org.churchit.dizimo.reports.utils.TypedDestinationTuple;

import java.io.File;
import java.util.*;

import static org.churchit.dizimo.reports.commons.ReportUtils.CURRENCY_INSTANCE;

public class TransactionRevenueResumeReport extends IncomeTransactionReporter {
    public static final String REPORT_FILE_PREFIX = "revenue-report";
    private static final String OFFERS_SECTION_TITLE = "Ofertas";
    private static final String TITHES_SECTION_TITLE = "Dízimos";
    private static final String TOTAL_SECTION_TITLE = "Total Geral";
    private static final String REPORT_NAME = "Relatório Sumarizado de Entradas";
    private static final String REPORT_SMALL_NAME = "ENTRADA GERAL";

    private static final MemoizationKey TOTAL_OFFERS_BUT_RECEIPT_MEMO =
            new MemoizationKey("TOTAL_OFFERS_BUT_RECEIPT_MEMO");

    public TransactionRevenueResumeReport(File file, TransactionDataDto transactionDataDto) throws IllegalArgumentException {
        super(file, transactionDataDto.clone(), REPORT_NAME, REPORT_SMALL_NAME);
    }

    @Override
    protected List<ListItem> buildReportBodySection() {
        ListItem tithesItem = buildTitheTypesSummarizeSection(
                transactionDataDto.getEnvelopeDtos(), TITHES_SECTION_TITLE);
        ListItem offersItem = buildOffersSummarizeSection();
        ListItem totalSummarizeItem = buildTotalSummarizeSection();

        return Arrays.asList(tithesItem, offersItem, totalSummarizeItem);
    }

    private ListItem buildTotalSummarizeSection() {
        Table table = new Table(equallyDistributedColumns(2));
        table.useAllAvailableWidth();

        final Double total = getTransactionTotalButReceipts();
        table = someSummarizeSectionFooter(table, total, getIncludesReceipts());

        ListItem listItem = new ListItem();
        listItem.add(new Paragraph(TOTAL_SECTION_TITLE));
        listItem.add(table);
        return listItem;
    }

    private Double getTransactionTotalButReceipts() {
        final Double offersButReceiptMemo = scoreValuesMap.get(TOTAL_OFFERS_BUT_RECEIPT_MEMO);
        final Double titheReceiptMemo = scoreValuesMap.get(TITHE_RECEIPT_SUM_MEMO);
        final Double titheMemo = scoreValuesMap.get(TOTAL_TITHE_MEMO);

        return offersButReceiptMemo + titheMemo - titheReceiptMemo;
    }

    private ListItem buildOffersSummarizeSection() {
        final float[] widths = {.4f, .2f, .4f};
        final Table table = new Table(UnitValue.createPercentArray(widths));
        table.useAllAvailableWidth();

        final OfferEnvelopeDto[] incomeList = transactionDataDto.getEnvelopeDtos();
        final Map<TypedDestinationTuple, Double> typedDestination = reduceOffersByTypedDestination(incomeList);

        final Double totalIncomeButReceipts = getTotalOffersButReceipts(typedDestination);

        reducedTypedOffersToCell(typedDestination).forEach(table::addCell);
        someSummarizeSectionFooter(table, totalIncomeButReceipts, getIncludesReceipts(), 3);

        ListItem listItem = new ListItem();
        listItem.add(new Paragraph(TransactionRevenueResumeReport.OFFERS_SECTION_TITLE));
        listItem.add(table);
        return listItem;
    }

    private Double getTotalOffersButReceipts(Map<TypedDestinationTuple, Double> typedDestination) {
        final Double scored = scoreValuesMap.get(TOTAL_OFFERS_BUT_RECEIPT_MEMO);
        if (scored != null) { return scored; }

        final Double sum = typedDestination.entrySet().stream()
                .filter(entry -> !entry.getKey().getInputTypeEnum().equals(InputTypeEnum.Receipt))
                .mapToDouble(Map.Entry::getValue)
                .sum();

        scoreValuesMap.put(TOTAL_OFFERS_BUT_RECEIPT_MEMO, sum);
        return sum;
    }

    private static List<Cell> reducedTypedOffersToCell(Map<TypedDestinationTuple, Double> typedOffers) {
        List<Cell> rows = new ArrayList<>();

        typedOffers.forEach((typedDestination, amount) -> {
            rows.add(new Cell().add(new Paragraph(typedDestination.getDestination())));
            rows.add(new Cell().add(new Paragraph(typedDestination.getInputTypeEnum().toString())));
            rows.add(new Cell().add(new Paragraph(CURRENCY_INSTANCE.format(amount))));
        });

        return rows;
    }

    private Map<String, Double> collectTypedOffersReduction(Map<InputTypeEnum, java.util.List<OfferVoDto>> typedOffers) {
        Map<String, Double> offerTypeIdentified = new HashMap<>();

        typedOffers.forEach((inputType, offerVoDtos) -> {
            final String inputTypeStr = inputType.toString();

            offerVoDtos.forEach(offerVoDto -> {
                String destination = offerVoDto.getDestination();
                destination = destination != null ? destination : OfferConstants.DEFAULT_OFFER_DESTINATION;
                destination += " - " + inputTypeStr;

                final double value = offerVoDto.getAmount() +
                        offerTypeIdentified.getOrDefault(destination, 0.d);

                offerTypeIdentified.put(destination, value);
            });

        });

        return offerTypeIdentified;
    }

    private Map<TypedDestinationTuple, Double> reduceOffersByTypedDestination(OfferEnvelopeDto[] incomeList) {
        final Map<TypedDestinationTuple, Double> result = new HashMap<>();

        Arrays.stream(incomeList).forEach(envelope -> {
            final InputTypeEnum inputType = InputTypeEnum.getInstance(envelope.getInputTypeId());

            Arrays.stream(envelope.getOffers()).forEach(
                    offerVoDto -> {
                        final TypedDestinationTuple typedDestination = new TypedDestinationTuple()
                                .setDestination(offerVoDto.getDestination())
                                .setInputTypeEnum(inputType);

                        result.merge(typedDestination, Double.valueOf(offerVoDto.getAmount()), Double::sum);
                    }
            );
        });

        return result;
    }

    private boolean getIncludesReceiptTithe() {
        return Arrays.stream(this.transactionDataDto.getEnvelopeDtos())
                .anyMatch(i -> i.getInputTypeId().equals(InputTypeEnum.Receipt.getId()) && i.getTitheAmount() != 0);
    }

    private boolean getIncludesReceiptOffer() {
        return Arrays.stream(this.transactionDataDto.getEnvelopeDtos())
                .anyMatch(i -> i.getInputTypeId().equals(InputTypeEnum.Receipt.getId()) && i.getOffers().length > 0);
    }

    private boolean getIncludesReceipts() {
        return Arrays.stream(this.transactionDataDto.getEnvelopeDtos())
                .anyMatch(i -> i.getInputTypeId().equals(InputTypeEnum.Receipt.getId()));
    }
}
