package org.churchit.dizimo.data.semantics;

public final class OfferConstants {

    public static final String DEFAULT_OFFER_DESTINATION = "<não especificado>";

    private OfferConstants() {
    }
}
