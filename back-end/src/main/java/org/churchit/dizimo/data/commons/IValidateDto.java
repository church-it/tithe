package org.churchit.dizimo.data.commons;

public interface IValidateDto {

    /**
     * Validates whether DTO has all necessary fields.
     * @return null if every field is valid; otherwise a message representing the error.
     */
    String assertInvalidFields();

}
