package org.churchit.dizimo.data.dtos;

import org.churchit.dizimo.data.commons.IValidateDto;
import org.churchit.dizimo.data.semantics.InputTypeEnum;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Objects;

import static org.churchit.dizimo.data.commons.ValidationUtils.fieldIsRequiredValidate;

public class OfferEnvelopeDto implements IValidateDto, Cloneable {
    private String fullName;
    private String comment;
    private Float titheAmount;
    private Integer inputTypeId;
    private String chequeNumber;
    private OfferVoDto[] offers;

    public String getFullName() {
        return fullName;
    }

    public OfferEnvelopeDto setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public Float getTitheAmount() {
        return titheAmount;
    }

    public OfferEnvelopeDto setTitheAmount(Float titheAmount) {
        this.titheAmount = titheAmount;
        return this;
    }

    public Integer getInputTypeId() {
        return inputTypeId;
    }

    public OfferEnvelopeDto setInputTypeId(Integer inputTypeId) {
        this.inputTypeId = inputTypeId;
        return this;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public OfferEnvelopeDto setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
        return this;
    }

    public OfferVoDto[] getOffers() {
        return offers;
    }

    public OfferEnvelopeDto setOffers(OfferVoDto[] offers) {
        this.offers = offers;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public OfferEnvelopeDto setComment(String comment) {
        this.comment = comment;
        return this;
    }

    @Override
    public String assertInvalidFields() {
        final Class<? extends OfferEnvelopeDto> clazz = this.getClass();
        if (titheAmount == null) {
            return fieldIsRequiredValidate("titheAmount", clazz);
        }

        if (inputTypeId == null) {
            return fieldIsRequiredValidate("inputTypeId", clazz);
        }

        final InputTypeEnum inputType = InputTypeEnum.getInstance(getInputTypeId());
        if (inputType == null) {
            return "inputTypeId \"" + getInputTypeId() + "\" is not a valid ID.";
        }

        if (inputType == InputTypeEnum.Cheque && chequeNumber == null) {
            return fieldIsRequiredValidate("chequeNumber", clazz);
        }

        if (StringUtils.hasLength(chequeNumber) && inputType != InputTypeEnum.Cheque) {
            return "set [chequeNumber] requires [inputType] equals \"" + InputTypeEnum.Cheque.toString()
                    + "\" (found: \"" + InputTypeEnum.Cheque.toString() + "\")";
        }

        if (StringUtils.isEmpty(fullName)) {
            return fieldIsRequiredValidate("fullName", clazz);
        }

        if (offers == null) {
            return fieldIsRequiredValidate("offers", clazz);
        }

        final OfferVoDto invalidOffer = Arrays.stream(offers)
                .filter(offerVoDto -> Objects.nonNull(offerVoDto.assertInvalidFields()))
                .findAny().orElse(null);

        if (invalidOffer != null) {
            return invalidOffer.assertInvalidFields();
        }

        if (comment == null) {
            return fieldIsRequiredValidate("comment", clazz);
        }

        return null;
    }
}
