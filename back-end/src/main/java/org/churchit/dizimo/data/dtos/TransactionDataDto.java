package org.churchit.dizimo.data.dtos;

import org.churchit.dizimo.data.commons.IValidateDto;

import java.util.Date;

import static org.churchit.dizimo.data.commons.ValidationUtils.fieldIsRequiredValidate;

public class TransactionDataDto implements IValidateDto, Cloneable {
    private SignatoryDto[] signatories;
    private OfferEnvelopeDto[] envelopeDtos;
    private Date date;
    private String hashValue;

    public Date getDate() {
        return date;
    }

    public TransactionDataDto setDate(Date date) {
        this.date = date;
        return this;
    }

    public SignatoryDto[] getSignatories() {
        return signatories;
    }

    public TransactionDataDto setSignatories(SignatoryDto[] signatories) {
        this.signatories = signatories;
        return this;
    }

    public OfferEnvelopeDto[] getEnvelopeDtos() {
        return envelopeDtos;
    }

    public TransactionDataDto setEnvelopeDtos(OfferEnvelopeDto[] envelopeDtos) {
        this.envelopeDtos = envelopeDtos;
        return this;
    }

    public String getHashValue() {
        return hashValue;
    }

    public TransactionDataDto setHashValue(String hashValue) {
        this.hashValue = hashValue;
        return this;
    }

    @Override
    public String assertInvalidFields() {
        final Class<? extends TransactionDataDto> clazz = this.getClass();

        if (getSignatories() == null) {
            return fieldIsRequiredValidate("signatories", clazz);
        }

        for (SignatoryDto signatoryDto : getSignatories()) {
            final String invalidMessage = signatoryDto.assertInvalidFields();
            if (invalidMessage != null) {
                return invalidMessage;
            }
        }

        if (getEnvelopeDtos() == null) {
            return fieldIsRequiredValidate("incomeList", clazz);
        }

        for (OfferEnvelopeDto itemDto : getEnvelopeDtos()) {
            final String invalidMessage = itemDto.assertInvalidFields();
            if (invalidMessage != null) {
                return invalidMessage;
            }
        }

        if (getDate() == null) {
            return fieldIsRequiredValidate("date", clazz);
        }

        if (getHashValue() == null) {
            return fieldIsRequiredValidate("hashValue", clazz);
        }

        return null;
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    public TransactionDataDto clone() {
        return new TransactionDataDto()
                .setDate((Date) date.clone())
                .setEnvelopeDtos(envelopeDtos.clone())
                .setSignatories(signatories.clone())
                .setHashValue(hashValue);
    }
}
