package org.churchit.dizimo.data.dtos;

import org.churchit.dizimo.data.commons.IValidateDto;

import static org.churchit.dizimo.data.commons.ValidationUtils.fieldIsRequiredValidate;

public class SignatoryDto implements IValidateDto, Cloneable {
    private String fullName;
    private String id;

    public String getFullName() {
        return fullName;
    }

    public SignatoryDto setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getId() {
        return id;
    }

    public SignatoryDto setId(String id) {
        this.id = id;
        return this;
    }

    @Override
    public String assertInvalidFields() {
        final Class<? extends SignatoryDto> clazz = this.getClass();

        if (getFullName() == null) {
            return fieldIsRequiredValidate("name", clazz);
        }

        if (getId() == null) {
            return fieldIsRequiredValidate("id", clazz);
        }

        return null;
    }
}
