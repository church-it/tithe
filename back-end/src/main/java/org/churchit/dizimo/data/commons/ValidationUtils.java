package org.churchit.dizimo.data.commons;

public final class ValidationUtils {
    private ValidationUtils() {
    }

    public static String fieldIsRequiredValidate(String fieldName, Class<?> clazz) {
        return "[" + fieldName + "] at " + clazz.getSimpleName() + " is required.";
    }
}
