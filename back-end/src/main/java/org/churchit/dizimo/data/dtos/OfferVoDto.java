package org.churchit.dizimo.data.dtos;

import org.churchit.dizimo.data.commons.IValidateDto;

import java.io.Serializable;

import static org.churchit.dizimo.data.commons.ValidationUtils.fieldIsRequiredValidate;

public class OfferVoDto implements Serializable, IValidateDto {
    private Float amount;
    private String destination;

    public Float getAmount() {
        return amount;
    }

    public OfferVoDto setAmount(Float amount) {
        this.amount = amount;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public OfferVoDto setDestination(String destination) {
        this.destination = destination;
        return this;
    }

    @Override
    public String assertInvalidFields() {
        final Class<? extends OfferVoDto> clazz = this.getClass();

        if (destination == null || destination.isEmpty()) {
            return fieldIsRequiredValidate("destination", clazz);
        }

        return null;
    }
}
