package org.churchit.dizimo.data.semantics;

import java.util.Arrays;

public enum InputTypeEnum {
     Money(0),
     Cheque(1),
    Receipt(2);

    private final int id;

    InputTypeEnum(int id) {
        this.id = id;
    }

    public static InputTypeEnum getInstance(int id) {
        final InputTypeEnum[] values = InputTypeEnum.values();
        final int index = Arrays.binarySearch(values, id, (a, b) -> {
            final InputTypeEnum a1 = (InputTypeEnum) a;
            final Integer b1 = (Integer) b;
            return a1.getId() - b1;
        });

        if (index < 0) { return null; }

        return values[index];
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return toLocaleString();
    }

    private String toLocaleString() {
        switch (this) {
            case Money:
                return "Dinheiro";
            case Cheque:
                return "Cheque";
            default:
                return "Recibo*";
        }
    }
}
