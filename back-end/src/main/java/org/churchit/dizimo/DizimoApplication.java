package org.churchit.dizimo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

import static org.churchit.dizimo.reports.commons.ReportUtils.TEMP_FOLDER;

@SpringBootApplication
public class DizimoApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(DizimoApplication.class);

    public static void main(String[] args) {
        prepareEnvironment();
        SpringApplication.run(DizimoApplication.class, args);
    }

    /**
     * @throws SecurityException if unable to create
     * {@link org.churchit.dizimo.reports.commons.ReportUtils#TEMP_FOLDER TEMP_FOLDER}
     * directory structure.
     */
    private static void prepareEnvironment() throws SecurityException {
        final File tempFolder = TEMP_FOLDER;
        final boolean mkdirs = tempFolder.mkdirs();
        final String absolutePath = tempFolder.getAbsolutePath();
        LOGGER.debug(absolutePath + " mkdirs returned: " + mkdirs);
    }

}
