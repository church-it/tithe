// @flow

import {OfferEnvelopeListActionsEnum} from "../actions/OfferEnvelopeListActions";
import {DetailedViewActionEnum} from "../actions/TransactionInputFormActions";
import {TransactionProgressActionEnum} from "../actions/TransactionStateActions";
import {OfferEnvelopeDto} from "../transactions/data/OfferEnvelopeDto";

const OUT_OF_RANGE_ERROR = new Error("id is out of range.");

const removeEnvelope = (list:OfferEnvelopeDto[], id:number): OfferEnvelopeDto[] => {
    return [...list.slice(0, id), ...list.slice(id+1)];
};

const addEnvelope = (list:OfferEnvelopeDto[], record:OfferEnvelopeDto): OfferEnvelopeDto[] => {
    return [...list, OfferEnvelopeDto.clone(record)];
};

const updateEnvelope = (list:OfferEnvelopeDto[], id:number, record:OfferEnvelopeDto): OfferEnvelopeDto[] => {
    if (id >= list.length) { throw OUT_OF_RANGE_ERROR; }

    return [...list.slice(0, id), OfferEnvelopeDto.clone(record), ...list.slice(id+1)];
};

function validateOpeningId(list: OfferEnvelopeDto[], id: number) {
    if (list.length <= id) {
        throw new RangeError();
    }
}

export const TRANSACTION_LIST_INIT = [];
const TransactionEnvelopeListReducer = (state:OfferEnvelopeDto[] = TRANSACTION_LIST_INIT, action:any): OfferEnvelopeDto[] => {
    switch (action.type) {
        case OfferEnvelopeListActionsEnum.ADD:
            return addEnvelope(state, action.record);
        case OfferEnvelopeListActionsEnum.REMOVE:
            return removeEnvelope(state, action.id);
        case OfferEnvelopeListActionsEnum.UPDATE:
            return updateEnvelope(state, action.id, action.record);
        case DetailedViewActionEnum.OPEN:
            // assertion only, real action reduced at ItemViewReducer
            validateOpeningId(state, action.id);
            return state;
        case TransactionProgressActionEnum.CLOSE:
            return [];
        default:
            return state;
    }
};

export default TransactionEnvelopeListReducer;