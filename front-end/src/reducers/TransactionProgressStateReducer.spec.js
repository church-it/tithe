// @flow

import TransactionProgressStateReducer from "./TransactionProgressStateReducer";
import {
    backwardStateAction,
    closeTransactionAction,
    editTransactionAction,
    forwardStateAction,
    openTransactionAction,
    TransactionProgressActionEnum
} from "../actions/TransactionStateActions";
import {REDUX_INIT} from "../redux-states/commons";
import {TransactionProgressStatesEnum} from "../redux-states/TransactionProgressState";
import {FOO_TRANSACTION_DATA} from "../containers";

describe('transactionProgressState reducer', () => {
    let before, after;

    it('should handle initial state', () => {
        before = undefined;
        after = TransactionProgressStatesEnum.CLOSED;
        expect(TransactionProgressStateReducer(before, REDUX_INIT)).toEqual(after);
    });


    it('should handle ' + TransactionProgressActionEnum.OPEN, () => {
        let dummyDate = new Date();
        before = TransactionProgressStatesEnum.OPEN;
        expect(() => TransactionProgressStateReducer(before, openTransactionAction(dummyDate))).toThrow();

        before = TransactionProgressStatesEnum.REVIEW;
        expect(() => TransactionProgressStateReducer(before, openTransactionAction(dummyDate))).toThrow();

        before = TransactionProgressStatesEnum.SIGN;
        expect(() => TransactionProgressStateReducer(before, openTransactionAction(dummyDate))).toThrow();

        before = TransactionProgressStatesEnum.WAITING_PUSH;
        expect(() => TransactionProgressStateReducer(before, openTransactionAction(dummyDate))).toThrow();

        before = TransactionProgressStatesEnum.FINISHED;
        expect(() => TransactionProgressStateReducer(before, openTransactionAction(dummyDate))).toThrow();

        before = TransactionProgressStatesEnum.CLOSED;
        after = TransactionProgressStatesEnum.OPEN;
        expect(TransactionProgressStateReducer(before, openTransactionAction(dummyDate))).toEqual(after);
    });

    it('should handle '+ TransactionProgressActionEnum.CLOSE, () => {
        after = TransactionProgressStatesEnum.CLOSED;

        before = TransactionProgressStatesEnum.OPEN;
        expect(TransactionProgressStateReducer(before, closeTransactionAction())).toEqual(after);

        before = TransactionProgressStatesEnum.REVIEW;
        expect(TransactionProgressStateReducer(before, closeTransactionAction())).toEqual(after);

        before = TransactionProgressStatesEnum.SIGN;
        expect(TransactionProgressStateReducer(before, closeTransactionAction())).toEqual(after);

        before = TransactionProgressStatesEnum.WAITING_PUSH;
        expect(() => TransactionProgressStateReducer(before, closeTransactionAction())).toThrow();

        before = TransactionProgressStatesEnum.FINISHED;
        expect(TransactionProgressStateReducer(before, closeTransactionAction())).toEqual(after);

        before = TransactionProgressStatesEnum.CLOSED;
        expect(TransactionProgressStateReducer(before, closeTransactionAction())).toEqual(after);
    });

    it('should handle '+ TransactionProgressActionEnum.EDIT, () => {
        let fooData = FOO_TRANSACTION_DATA(new Date());

        before = TransactionProgressStatesEnum.OPEN;
        expect(() => TransactionProgressStateReducer(before, editTransactionAction(fooData))).toThrow();

        before = TransactionProgressStatesEnum.REVIEW;
        expect(() => TransactionProgressStateReducer(before, editTransactionAction(fooData))).toThrow();

        before = TransactionProgressStatesEnum.SIGN;
        expect(() => TransactionProgressStateReducer(before, editTransactionAction(fooData))).toThrow();

        before = TransactionProgressStatesEnum.WAITING_PUSH;
        expect(() => TransactionProgressStateReducer(before, editTransactionAction(fooData))).toThrow();

        before = TransactionProgressStatesEnum.FINISHED;
        expect(() => TransactionProgressStateReducer(before, editTransactionAction(fooData))).not.toThrow();

        before = TransactionProgressStatesEnum.CLOSED;
        expect(() => TransactionProgressStateReducer(before, editTransactionAction(fooData))).not.toThrow();
    });

    it('should handle ' + TransactionProgressActionEnum.FORWARD, () => {
        before = TransactionProgressStatesEnum.CLOSED;
        expect(() => TransactionProgressStateReducer(before, forwardStateAction())).toThrow();

        before = TransactionProgressStatesEnum.OPEN;
        after = TransactionProgressStatesEnum.REVIEW;
        expect(TransactionProgressStateReducer(before, forwardStateAction())).toEqual(after);

        before = TransactionProgressStatesEnum.REVIEW;
        after = TransactionProgressStatesEnum.SIGN;
        expect(TransactionProgressStateReducer(before, forwardStateAction())).toEqual(after);

        before = TransactionProgressStatesEnum.SIGN;
        after = TransactionProgressStatesEnum.WAITING_PUSH;
        expect(TransactionProgressStateReducer(before, forwardStateAction())).toEqual(after);

        before = TransactionProgressStatesEnum.WAITING_PUSH;
        after = TransactionProgressStatesEnum.FINISHED;
        expect(TransactionProgressStateReducer(before, forwardStateAction())).toEqual(after);

        before = TransactionProgressStatesEnum.FINISHED;
        expect(() => TransactionProgressStateReducer(before, forwardStateAction())).toThrow();
    });

    it('should handle ' + TransactionProgressActionEnum.BACKWARD, () => {
        before = TransactionProgressStatesEnum.CLOSED;
        expect(() => TransactionProgressStateReducer(before, backwardStateAction())).toThrow();

        before = TransactionProgressStatesEnum.OPEN;
        expect(() => TransactionProgressStateReducer(before, backwardStateAction())).toThrow();

        before = TransactionProgressStatesEnum.REVIEW;
        after = TransactionProgressStatesEnum.OPEN;
        expect(TransactionProgressStateReducer(before, backwardStateAction())).toEqual(after);

        before = TransactionProgressStatesEnum.SIGN;
        after = TransactionProgressStatesEnum.REVIEW;
        expect(TransactionProgressStateReducer(before, backwardStateAction())).toEqual(after);

        before = TransactionProgressStatesEnum.WAITING_PUSH; // jumped a state
        after = TransactionProgressStatesEnum.REVIEW;
        expect(TransactionProgressStateReducer(before, backwardStateAction())).toEqual(after);

        before = TransactionProgressStatesEnum.FINISHED;
        expect(() => TransactionProgressStateReducer(before, backwardStateAction())).toThrow();
    });
});