// @flow

import TransactionDataReducer, {TRANSACTION_DATA_INIT} from "./TransactionDataReducer";
import {REDUX_INIT, REDUX_INIT_TYPE_PREFIX} from "../redux-states/commons";
import {
    editTransactionAction,
    openTransactionAction,
    TransactionProgressActionEnum
} from "../actions/TransactionStateActions";
import {FOO_SIGNATORIES, FOO_TITHE} from "../containers";
import type {TransactionData} from "../transactions/data/TransactionDataDto";

describe('TransactionDataReducer reducer', function () {
    it('should handle ' + REDUX_INIT_TYPE_PREFIX, function () {
        let before = undefined;
        expect(TransactionDataReducer(before, REDUX_INIT)).toEqual(TRANSACTION_DATA_INIT);
    });

    it('should handle ' + TransactionProgressActionEnum.EDIT, function () {
        let foo_data:()=>TransactionData = () => ({
            hashValue:"test", date:new Date(), signatories: FOO_SIGNATORIES(), envelopeDtos: [FOO_TITHE()]
        });

        let before = undefined;
        let after:TransactionData = foo_data();
        after.signatories = null;
        expect(TransactionDataReducer(before, editTransactionAction(foo_data()))).toEqual(after);

        before = TRANSACTION_DATA_INIT;
        after = foo_data();
        after.signatories = null;
        expect(TransactionDataReducer(before, editTransactionAction(foo_data()))).toEqual(after);
    });

    it('should produce new HASHES on ' + TransactionProgressActionEnum.OPEN, function () {
        let defaultOpenReduction = (before) => TransactionDataReducer(before, openTransactionAction(new Date()));

        expect(defaultOpenReduction(TRANSACTION_DATA_INIT).hashValue).toEqual(TRANSACTION_DATA_INIT.hashValue);
    });
});