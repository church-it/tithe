// @flow

import TransactionEnvelopeListReducer, {TRANSACTION_LIST_INIT} from "./TransactionEnvelopeListReducer";
import TransactionSignatoriesReducer, {TRANSACTION_SIGNATORIES_INIT} from "./TransactionSignatoriesReducer";
import TransactionDateReducer, {TRANSACTION_DATE_INIT} from "./TransactionDateReducer";
import type {TransactionData} from "../transactions/data/TransactionDataDto";

let TransactionStateActions = require("../actions/TransactionStateActions");

function newHashValue() {
    return (+new Date()).toString(36).slice(-8);
}

// not frozen to allow hashValue update
export const TRANSACTION_DATA_INIT:TransactionData = {
    envelopeDtos: TRANSACTION_LIST_INIT,
    signatories: TRANSACTION_SIGNATORIES_INIT,
    date: TRANSACTION_DATE_INIT,
    hashValue: newHashValue(),
};

function composedReducers(state, action):TransactionData {
    let envelopeList = TransactionEnvelopeListReducer(state.envelopeDtos, action);
    let signatories = TransactionSignatoriesReducer(state.signatories, action);
    let date = TransactionDateReducer(state.date, action);

    if(action.type === TransactionStateActions.TransactionProgressActionEnum.OPEN) {
        TRANSACTION_DATA_INIT.hashValue = newHashValue();
    }

    return {
        envelopeDtos: envelopeList,
        signatories,
        date,
        hashValue: state.hashValue,
    }
}

export default TransactionDataReducer;

function TransactionDataReducer(state: TransactionData = TRANSACTION_DATA_INIT, action: any): TransactionData {
    if (action.type === TransactionStateActions.TransactionProgressActionEnum.EDIT) {
        let data: TransactionData = action.data;

        return {
            envelopeDtos: data.envelopeDtos,
            signatories: null,
            hashValue: data.hashValue,
            date: data.date,
        }
    }

    return composedReducers(state, action);
}