// @flow

import {closeEditionAction, DetailedViewActionEnum, openEditionAction} from "../actions/TransactionInputFormActions";
import {titheFromKey} from "./TransactionEnvelopeListReducer.spec";

import DetailedViewReducer from './DetailedViewReducer';
import TransactionEnvelopeListReducer from "./TransactionEnvelopeListReducer";
import {REDUX_INIT} from "../redux-states/commons";
import type {DetailedViewState} from "../redux-states/DetailedViewTypes";
import {OfferEnvelopeDto} from "../transactions/data/OfferEnvelopeDto";


describe('TransactionEnvelopeListReducer reducer', () => {
    let before, after;

    let encapsulatedState = (currentRecordId:?number):DetailedViewState => ({currentRecordId});

    it('should handle initial state', () => {
        before = undefined;
        after = null;
        expect(DetailedViewReducer(before, REDUX_INIT).currentRecordId).toEqual(after);
    });

    it('should handle ' + DetailedViewActionEnum.OPEN, () => {
        let titheA = titheFromKey(Math.random());
        let titheB = titheFromKey(Math.random());
        let tithes:OfferEnvelopeDto[] = [titheA, titheB];

        before = encapsulatedState(null);
        after = encapsulatedState(0);
        expect(DetailedViewReducer(before, openEditionAction(0))).toEqual(after);
        expect(() => TransactionEnvelopeListReducer(tithes, openEditionAction(0))).not.toThrow();

        before = encapsulatedState(0);
        after = encapsulatedState(1);
        expect(DetailedViewReducer(before, openEditionAction(1))).toEqual(after);
        expect(() => TransactionEnvelopeListReducer(tithes, openEditionAction(1))).not.toThrow();

        expect(() => TransactionEnvelopeListReducer([], openEditionAction(3))).toThrow();
    });

    it('should handle ' + DetailedViewActionEnum.CLOSE, () => {
        before = encapsulatedState(null);
        expect(() => DetailedViewReducer(before, closeEditionAction())).toThrow();

        before = DetailedViewReducer(encapsulatedState(null), openEditionAction(0));
        expect(DetailedViewReducer(before, closeEditionAction())).toEqual(encapsulatedState(null));
    });
});