// @flow

import {REDUX_INIT} from "../redux-states/commons";
import TransactionSignatoriesReducer from "./TransactionSignatoriesReducer";
import {commitSignatoriesAction, CommitSignatoriesActionEnum} from "../actions/TransactionSignatoriesActions";
import {
    backwardStateAction,
    openTransactionAction,
    TransactionProgressActionEnum
} from "../actions/TransactionStateActions";

describe('signatories reducer', () => {
    let before;
    let after;

    it('should handle initial state', () => {
        before = undefined;
        after = null;
        expect(TransactionSignatoriesReducer(before, REDUX_INIT)).toEqual(after);
    });

    let fooSignatories = () => [
        {fullName:"foo1", id:"1"},
        {fullName:"foo2", id:"2"},
    ];

    it('should handle ' + CommitSignatoriesActionEnum.COMMIT, () => {
        before = null;
        after = fooSignatories();
        expect(TransactionSignatoriesReducer(before, commitSignatoriesAction(fooSignatories()))).toEqual(after);

        before = fooSignatories();
        expect(() => TransactionSignatoriesReducer(before, commitSignatoriesAction(fooSignatories()))).toThrow();
    });

    it('should handle ' + TransactionProgressActionEnum.BACKWARD, function () {
        before = null;
        after = null;
        expect(TransactionSignatoriesReducer(before, backwardStateAction())).toEqual(after);

        before = [];
        after = null;
        expect(TransactionSignatoriesReducer(before, backwardStateAction())).toEqual(after);
    });

    it('should handle ' + TransactionProgressActionEnum.OPEN, function () {
        before = null;
        after = null;
        expect(TransactionSignatoriesReducer(before, openTransactionAction(new Date()))).toEqual(after);

        before = [];
        after = null;
        expect(TransactionSignatoriesReducer(before, openTransactionAction(new Date()))).toEqual(after);
    });
});