// @flow

import {combineReducers} from 'redux';
import TransactionProgressStateReducer from "./TransactionProgressStateReducer";
import DetailedViewReducer from "./DetailedViewReducer";
import TransactionDataReducer from "./TransactionDataReducer";
import type {TransactionProgressStateType} from "../redux-states/TransactionProgressState";
import type {DetailedViewState} from "../redux-states/DetailedViewTypes";
import type {TransactionData} from "../transactions/data/TransactionDataDto";

export default combineReducers({
    transactionData: TransactionDataReducer,
    transactionProgressState: TransactionProgressStateReducer,
    transactionItemView: DetailedViewReducer,
});

export type State = {
    transactionData: TransactionData,
    transactionProgressState: TransactionProgressStateType,
    transactionItemView: DetailedViewState,
};
