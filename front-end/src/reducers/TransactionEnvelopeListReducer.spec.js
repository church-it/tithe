//@flow

import TransactionEnvelopeListReducer from "./TransactionEnvelopeListReducer";
import {
    addOfferEnvelopeAction,
    OfferEnvelopeListActionsEnum,
    removeOfferEnvelopeAction,
    updateOfferEnvelopeAction
} from "../actions/OfferEnvelopeListActions";
import {REDUX_INIT} from "../redux-states/commons";
import {InputTypeEnum} from "../transactions/data/OfferVo";
import {closeTransactionAction, TransactionProgressActionEnum} from "../actions/TransactionStateActions";
import {FOO_TITHE} from "../containers";
import {OfferEnvelopeDto} from "../transactions/data/OfferEnvelopeDto";

export function titheFromKey(randomKey: number):OfferEnvelopeDto {
    randomKey =  Math.floor(randomKey * 1000)+1;

    let date: Date = new Date(randomKey);
    let fullName: string = "fullName"+randomKey;
    let inputTypeId = InputTypeEnum.MONEY;
    let titheAmount = randomKey;

    return new OfferEnvelopeDto({fullName, comment:"comment", offers: [], inputTypeId, titheAmount, date, chequeNumber:null});
}

describe('TransactionEnvelopeListReducer reducer', () => {
    let before, after;

    it('should handle initial state', () => {
        expect(TransactionEnvelopeListReducer(undefined, REDUX_INIT)).toEqual([]);
    });

    it('should handle '+OfferEnvelopeListActionsEnum.ADD, () => {
        let keyA = Math.random();
        let stateA = TransactionEnvelopeListReducer([], addOfferEnvelopeAction(titheFromKey(keyA)));
        expect(stateA).toEqual([titheFromKey(keyA)]);

        let keyB = Math.random();
        let stateB = TransactionEnvelopeListReducer(stateA, addOfferEnvelopeAction(titheFromKey(keyB)));
        expect(stateB).toEqual([titheFromKey(keyA), titheFromKey(keyB)]);

        // besides transactionEnvelopes, nothing else should change
        stateA = stateB = null;
        expect(stateA).toEqual(stateB);
    });

    it('should handle '+OfferEnvelopeListActionsEnum.REMOVE, () => {
        let tithes = [titheFromKey(Math.random()), titheFromKey(Math.random())];
        let titheA = tithes[0];
        let titheB = tithes[1];

        let stateA = TransactionEnvelopeListReducer(tithes, removeOfferEnvelopeAction(0));
        expect(stateA).toEqual([titheB]);

        let stateB = TransactionEnvelopeListReducer(tithes, removeOfferEnvelopeAction(1));
        expect(stateB).toEqual([titheA]);

        stateA = stateB = null;
        expect(stateA).toEqual(stateB);
    });

    it('should handle' + OfferEnvelopeListActionsEnum.UPDATE, () => {
        let before = [titheFromKey(Math.random()), titheFromKey(Math.random())];

        let after = [FOO_TITHE(), before[1]];
        expect(TransactionEnvelopeListReducer(before, updateOfferEnvelopeAction(0, FOO_TITHE()))).toEqual(after);

        after = [before[0], FOO_TITHE()];
        expect(TransactionEnvelopeListReducer(before, updateOfferEnvelopeAction(1, FOO_TITHE()))).toEqual(after);

        expect(() => TransactionEnvelopeListReducer(before, updateOfferEnvelopeAction(2, FOO_TITHE()))).toThrow();
    });

    it('should handle' + TransactionProgressActionEnum.CLOSE, () => {
        before = [titheFromKey(Math.random()), titheFromKey(Math.random())];
        after = [];
        expect(TransactionEnvelopeListReducer(before, closeTransactionAction())).toEqual(after);
    });

    it('should produce immutable states', () => {
        let titheA = titheFromKey(Math.random());
        let titheB = titheFromKey(Math.random());
        let tithes = [titheA, titheB];

        let stateA = TransactionEnvelopeListReducer(tithes, removeOfferEnvelopeAction(0));
        expect(tithes).toEqual([titheA, titheB]);

        let titheC = titheFromKey(Math.random());
        let stateB = TransactionEnvelopeListReducer(tithes, addOfferEnvelopeAction(titheC));
        titheC.titheAmount = 0;
        expect(tithes).toEqual([titheA, titheB]);
        expect(stateB.find(x=>x.fullName===titheC.fullName)).not.toEqual(titheC);
    });
});