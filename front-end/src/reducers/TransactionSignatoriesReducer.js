// @flow
import type {SignatoryVo} from "../transactions/data/SignatoryVo";
import {CommitSignatoriesActionEnum} from "../actions/TransactionSignatoriesActions";
import {TransactionProgressActionEnum} from "../actions/TransactionStateActions";

function UNEXPECTED_COMMIT_ERROR(currentState:any) {
    return new Error("Unexpected commit:\n" + JSON.stringify(currentState, null, 2));
}

export const TRANSACTION_SIGNATORIES_INIT = null;

function TransactionSignatoriesReducer (state:?SignatoryVo[] = TRANSACTION_SIGNATORIES_INIT, action:any):?SignatoryVo[] {
    switch (action.type) {
        case CommitSignatoriesActionEnum.COMMIT:
            if (state === null) {
                return action.signatories;
            }

            throw UNEXPECTED_COMMIT_ERROR(state);
        case TransactionProgressActionEnum.BACKWARD:
        case TransactionProgressActionEnum.OPEN:
            return null;
        default:
            return state;
    }
}

export default TransactionSignatoriesReducer;