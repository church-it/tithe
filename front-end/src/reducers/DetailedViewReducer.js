// @flow

import type {DetailedViewState} from "../redux-states/DetailedViewTypes";
import {NEW_TRANSACTION_FORM_REDUX_STATE,} from "../redux-states/DetailedViewTypes";
import {DetailedViewActionEnum} from "../actions/TransactionInputFormActions";
import {TransactionProgressActionEnum} from "../actions/TransactionStateActions";

const UNEXPECTED_CLOSE_ERROR:Error = new Error("Unexpected close.");

const DetailedViewReducer = (state:DetailedViewState = NEW_TRANSACTION_FORM_REDUX_STATE,
                             action:any): DetailedViewState => {
    switch (action.type) {
        case DetailedViewActionEnum.OPEN:
            return {
                ...state,
                currentRecordId: action.id
            };
        case DetailedViewActionEnum.CLOSE:
            if (state.currentRecordId === null) { throw UNEXPECTED_CLOSE_ERROR }
            return {
                ...state,
                currentRecordId: null
            };
        case TransactionProgressActionEnum.BACKWARD:
        case TransactionProgressActionEnum.FORWARD:
        case TransactionProgressActionEnum.CLOSE:
        case TransactionProgressActionEnum.OPEN:
            return {
                ...state,
                currentRecordId: null
            };
        default:
            return state;
    }
};

export default DetailedViewReducer;