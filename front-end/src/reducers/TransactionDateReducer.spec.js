// @flow

import TransactionDateReducer, {TRANSACTION_DATE_INIT} from "./TransactionDateReducer";
import {REDUX_INIT} from "../redux-states/commons";
import {
    closeTransactionAction,
    openTransactionAction,
    TransactionProgressActionEnum
} from "../actions/TransactionStateActions";

describe("TransactionDateReducer reducer", () => {
    it('should handle init state', function () {
        expect(TransactionDateReducer(undefined, REDUX_INIT)).toEqual(TRANSACTION_DATE_INIT);
    });

    it('should handle ' + TransactionProgressActionEnum.OPEN, function () {
        let date = new Date();
        expect(TransactionDateReducer(TRANSACTION_DATE_INIT, openTransactionAction(date))).toEqual(date);
    });

    it('should handle ' + TransactionProgressActionEnum.CLOSE, function () {
        expect(TransactionDateReducer(TRANSACTION_DATE_INIT, closeTransactionAction())).toEqual(null);
    });
});