// @flow

import type {TransactionProgressStateType} from "../redux-states/TransactionProgressState";
import {TRANSACTION_STATE_INIT, TransactionProgressStatesEnum} from "../redux-states/TransactionProgressState";
import {REDUX_INIT_TYPE_PREFIX} from "../redux-states/commons";
import {TransactionProgressActionEnum} from "../actions/TransactionStateActions";

const UNEXPECTED_MOVEMENT_ERROR = (where) => new Error("Unexpected movement @ " + where);

function nextState(state: TransactionProgressStateType):TransactionProgressStateType {
    switch (state) {
        case TransactionProgressStatesEnum.OPEN:
            return TransactionProgressStatesEnum.REVIEW;
        case TransactionProgressStatesEnum.REVIEW:
            return TransactionProgressStatesEnum.SIGN;
        case TransactionProgressStatesEnum.SIGN:
            return TransactionProgressStatesEnum.WAITING_PUSH;
        case TransactionProgressStatesEnum.WAITING_PUSH:
            return TransactionProgressStatesEnum.FINISHED;
        default:
            throw UNEXPECTED_MOVEMENT_ERROR( "nextState");
    }
}

function backState(state: TransactionProgressStateType):TransactionProgressStateType {
    switch (state) {
        case TransactionProgressStatesEnum.REVIEW:
            return TransactionProgressStatesEnum.OPEN;
        case TransactionProgressStatesEnum.SIGN:
            return TransactionProgressStatesEnum.REVIEW;
        case TransactionProgressStatesEnum.WAITING_PUSH:
            return TransactionProgressStatesEnum.REVIEW;
        default:
            throw UNEXPECTED_MOVEMENT_ERROR("backState");
    }
}

function openState(state: TransactionProgressStateType):TransactionProgressStateType {
    if (state !== TransactionProgressStatesEnum.CLOSED) {
        throw UNEXPECTED_MOVEMENT_ERROR("openState");
    }

    return TransactionProgressStatesEnum.OPEN;
}

function closeState(state: TransactionProgressStateType):TransactionProgressStateType {
    if (state !== TransactionProgressStatesEnum.WAITING_PUSH) {
        return TransactionProgressStatesEnum.CLOSED;
    } else {
        throw UNEXPECTED_MOVEMENT_ERROR("closeState");
    }
}

function editState(state: TransactionProgressStateType):TransactionProgressStateType {
    switch (state) {
        case TransactionProgressStatesEnum.FINISHED:
        case TransactionProgressStatesEnum.CLOSED:
            return TransactionProgressStatesEnum.REVIEW;
        default:
            throw UNEXPECTED_MOVEMENT_ERROR("editState");
    }
}

const TransactionProgressStateReducer = (state:TransactionProgressStateType = TRANSACTION_STATE_INIT, action:any)
    : TransactionProgressStateType => {

    if ( action.type.toLocaleLowerCase().startsWith(REDUX_INIT_TYPE_PREFIX)) { return state; }

    switch (action.type) {
        case TransactionProgressActionEnum.CLOSE:
            return closeState(state);
        case TransactionProgressActionEnum.OPEN:
            return openState(state);
        case TransactionProgressActionEnum.EDIT:
            return editState(state);
        case TransactionProgressActionEnum.FORWARD:
            return nextState(state);
        case TransactionProgressActionEnum.BACKWARD:
            return backState(state);
        default:
            return state;
    }

};

export default TransactionProgressStateReducer;