import {TransactionProgressActionEnum} from "../actions/TransactionStateActions";

export const TRANSACTION_DATE_INIT = null;
export default (state:?Date = TRANSACTION_DATE_INIT, action:any):?Date => {
    switch (action.type) {
        case TransactionProgressActionEnum.OPEN:
            return action.date;
        case TransactionProgressActionEnum.CLOSE:
            return TRANSACTION_DATE_INIT;
        default:
            return state;
    }
}