// @flow

export type DetailedViewState = {
    currentRecordId: ?number
};
export const NEW_TRANSACTION_FORM_REDUX_STATE:DetailedViewState = {
    currentRecordId: null,
};
