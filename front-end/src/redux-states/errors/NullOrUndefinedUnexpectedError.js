export class NullOrUndefinedUnexpectedError implements Error {

    static assertOrThrow(var1:?any):any {
        if (var1 === null || var1 === undefined) {
            throw new NullOrUndefinedUnexpectedError();
        }

        return var1;
    }
}