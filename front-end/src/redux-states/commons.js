// @flow

export const REDUX_INIT_TYPE_PREFIX = "@@redux";
export const REDUX_INIT = Object.freeze({type: REDUX_INIT_TYPE_PREFIX});