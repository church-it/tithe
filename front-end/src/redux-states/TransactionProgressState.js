// @flow

export const TransactionProgressStatesEnum = Object.freeze({
    OPEN: "OPEN-TRANSACTION-STATE",
    CLOSED: "CLOSED-TRANSACTION-STATE",
    REVIEW: "REVIEW_TO_SIGN_STATE",
    SIGN: "SIGN_TRANSACTION_STATE",
    WAITING_PUSH: "PUSH_TRANSACTION_STATE",
    FINISHED: "COMMITTED_TRANSACTION_STATE",
});

export type TransactionProgressStateType = $Values<typeof TransactionProgressStatesEnum>;

export const TRANSACTION_STATE_INIT:TransactionProgressStateType = TransactionProgressStatesEnum.CLOSED;