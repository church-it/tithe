// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

import './main-index.css';
import App from './components/main/App';
import reducers from './reducers';

const store = createStore(
    reducers
);

const element:any = document.getElementById('root');

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    element
);
