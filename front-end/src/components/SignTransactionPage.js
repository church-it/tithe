// @flow
import React from 'react';
import Form from "react-bootstrap/Form";
import "./styles/SignTransactionPage.css"
import StateTitleContainer from "../containers/items/StateTitleContainer";
import type {SignatoryVo} from "../transactions/data/SignatoryVo";
import {ProceedButton} from "./items/ProceedButton";
import {FINISH_LABEL} from "./literals/FormCommons";
import {Signatories} from "./items/signatories/Signatories";
import {validateCpf} from "./commons/LocaleUtils";
import ValidationErrorModal from "./items/input-form/ValidationErrorModal";

const signatories_label = "Signatários"; // pt_BR
const invalid_ids_messages = ["Um ou mais CPFs são inválidos, por favor verifique."]; // pt_BR
const null_cpf_tip_label = "Obs.: Se não quiser fornecer o CPF, você pode preencher com zeros.";

export type Props = {
    onCommit: (signatories:SignatoryVo[]) => any
}
type ClassState = {
    signatories: SignatoryVo[],
    idsValidity: boolean[],
};

export class SignTransactionPage extends React.Component<Props, ClassState> {
    state = {
        signatories: [
            {fullName: "Eduardo Pacheco", id: "-"},
            {fullName: "?", id: "-"},
        ],
        idsValidity: [false, false],
    };

    validationModalRef:any = React.createRef();

    constructor(props:Props) {
        super(props);
        this.addSignatory = this.addSignatory.bind(this);
        this.removeSignatory = this.removeSignatory.bind(this);
        this.updateSignatory = this.updateSignatory.bind(this);
        this.commitSignatories = this.commitSignatories.bind(this);
    }

    render() {
        let {signatories, idsValidity} = this.state;


        return <div className="sign-page-wrap">
            <Form>
                <StateTitleContainer title={signatories_label} />
                <Signatories idsValidity={idsValidity} signatories={signatories}
                             onAddSignatory={this.addSignatory}
                             onRemoveSignatory={this.removeSignatory}
                             onUpdateSignatory={this.updateSignatory} />
                <ProceedButton canProceed={true} label={FINISH_LABEL} variant="danger"
                               onProceed={this.commitSignatories} />
               <ValidationErrorModal ref={this.validationModalRef}
                                     messages={invalid_ids_messages}
                                     tip={null_cpf_tip_label}/>
            </Form>
        </div>
    }

    addSignatory = () => {
        let {signatories: oldSignatories, idsValidity: oldIdsValidity} = this.state;

        let signatories = [...oldSignatories, {fullName: "", id: ""}];

        let idsValidity = [...oldIdsValidity, false];

        this.setState({signatories, idsValidity});
    };

    removeSignatory = (key:number) => {
        let {signatories: oldSignatories, idsValidity: oldIdsValidity} = this.state;

        let signatories = [...oldSignatories.slice(0,key), ...oldSignatories.slice(key+1)];

        let idsValidity = [...oldIdsValidity.slice(0,key), ...oldIdsValidity.slice(key+1)];

        this.setState({signatories, idsValidity});
    };

    updateSignatory = (key:number, signatory:SignatoryVo) => {
        let {signatories: oldSignatories, idsValidity: oldIdsValidity} = this.state;

        let signatories = [...oldSignatories.slice(0,key), signatory, ...oldSignatories.slice(key+1)];

        let idsValidity = [...oldIdsValidity.slice(0,key), this.validateId(signatory.id), ...oldIdsValidity.slice(key+1)];

        this.setState({signatories, idsValidity});
    };

    commitSignatories = () => {
        let {onCommit} = this.props;
        let {idsValidity, signatories} = this.state;
        
        if (idsValidity.indexOf(false) !== -1) {
            const errorModal:ValidationErrorModal = this.validationModalRef.current;
            errorModal.show();
        } else {
            onCommit(signatories);
        }
    };

    validateId(id:string):boolean {
        return validateCpf(id);
    }
}