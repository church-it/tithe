import {validateCpf} from "./LocaleUtils";

describe('LocaleUtils testing', function () {
    it('should properly validate CPFs', function () {
        expect(validateCpf("733.940.171.72")).toBeTruthy();
        expect(validateCpf("733.940.171.02")).toBeFalsy();
        expect(validateCpf("733.940.171.70")).toBeFalsy();
        expect(validateCpf("..-")).toBeFalsy();
    });
});