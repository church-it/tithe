// @flow
import React from "react";
import "../styles/PrependedInput.css"

export type PrependedInputProps = {
    value:any,
    onChange:(event:Event) => any,
    onKeyDown:(event:KeyboardEvent) => any,
    placeholder:string,
    title:string,
    type:string,
    prependWidth?:number,
    className:string,
    style:any,
}
export class PrependedInput extends React.Component<PrependedInputProps> {
    static defaultProps = {
        placeholder: "",
        type: "text",
        onKeyDown: () => {},
        className: "",
        style: {},
    };

    inputRef:any = React.createRef();


    constructor(props:PrependedInputProps) {
        super(props);
        this.focus = this.focus.bind(this);
    }

    render() {
        let {onChange, value, placeholder, title, type, prependWidth, onKeyDown,
            style, className, ...remaining} = this.props;
        let prependStyle = prependWidth !== undefined ? {minWidth: prependWidth + "px"} : {};
        
        return <div style={style} className={"prepended-input-wrap " + className}>
            <div className="prepend" style={prependStyle}>{title}</div>
            <input {...remaining} className="input"
                   type={type} placeholder={placeholder} ref={this.inputRef}
                   value={value} onChange={onChange} onKeyDown={onKeyDown}/>
        </div>
    }

    focus = () => {
        this.inputRef.current.focus();
    }
}