// @flow
import React, {Component} from "react";
import Button from "react-bootstrap/Button";
import Octicon, {Icon, Pencil} from "@primer/octicons-react";
import "../styles/BipartiteButton.css";

type DownloadButtonProps = {
    children: ?any,
    onClick?: (e:MouseEvent) => any,
    icon: Icon,
    width: number,
    variant: string,
};
export default class BipartiteButton extends Component<DownloadButtonProps> {
    static defaultProps = {
        width: 25,
        variant: "primary",
        icon: Pencil,
};

    render() {
        let {children, onClick, icon, width, variant} = this.props;


        return <Button className="download-button-wrap" size="sm" variant={variant} onClick={onClick}>
            <div children={children} className="download-button-left"/>
            <div className="download-button-right">
                <Octicon icon={icon} width={width} verticalAlign='middle'/>
            </div>
        </Button>
    }
}