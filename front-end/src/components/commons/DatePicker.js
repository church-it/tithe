// @flow

import React from "react";

type Props = {onChange: (date:Date) => any, valueAsDate: Date};
class DatePicker extends React.Component<Props> {
    static defaultProps = {
        onChange: () => {}
    };

    render() {
        let {onChange, valueAsDate, ...props} = this.props;

        return <input value={DatePicker.dateToISOFormat(valueAsDate)} {...props} type="date"
                      className="datePicker" onChange={e => onChange(e.target.valueAsDate)} />
    }

    static dateToISOFormat(date:Date) {
        return date.toISOString().slice(0, 10);
    }
}

export default DatePicker;