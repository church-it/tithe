// @flow

export const CURRENCY_SIGN = "R$"; // pt_BR
export const CPF_MAX_NUMBER_LENGTH = 11;

export function amountToCurrency(amount:number, withCurrency:?boolean):string {
    return (withCurrency ? CURRENCY_SIGN + " " : "") + amount.toFixed(2);
}


/**
 * @author Maycon Alves
 * @copyright https://medium.com/trainingcenter/mascara-de-cpf-com-react-javascript-a07719345c93
 * @param value
 * @returns {string}
 * @constructor
 */
export function CpfMask (value:string) {
    return value
        // substitui qualquer caractere que nao seja numero por nada
        .replace(/\D/g, '')
        // captura 2 grupos de número o primeiro de 3 e o segundo de 1, apos capturar o primeiro grupo ele adiciona
        // um ponto antes do segundo grupo de números
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d{1,2})/, '$1-$2')
        // captura 2 números seguidos de um traço e não deixa ser digitado mais nada
        .replace(/(-\d{2})\d+?$/, '$1');
}

function getCpfDigits(cpfString) {
// tudo que não é número vira nada.
    cpfString = cpfString.replace(/\D/g, '');

    let cpfDigits: number[] = [];
    for (let i = 0; i < cpfString.length; ++i) {
        cpfDigits = [...cpfDigits, parseInt(cpfString.charAt(i))];
    }

    return cpfDigits;
}

function cpfReduction(digits, length) {
    let acc = 0;
    for (let i = 0; i < length; i++) {
        acc += digits[i] * (length + 1 - i); // ..., 10, 9, ..., 2
    }

    let mod = acc * 10 % 11;
    // se o resto for 10, o digito é 0
    mod = mod % 10;

    return mod;
}

export function validateCpf (cpfString:string) {
    let digits = getCpfDigits(cpfString);

    return digits.length === 11 && cpfReduction(digits, 9) === digits[9]
        && cpfReduction(digits, 10) === digits[10];
}