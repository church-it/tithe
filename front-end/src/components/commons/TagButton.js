// @flow
import React, {Component} from "react";
import Octicon, {Icon} from "@primer/octicons-react";
import Button from "react-bootstrap/Button";
import "../styles/TagButton.css";

export class TagButton extends Component<{ icon: Icon, width: number, marginLeft:string }> {
    static defaultProps = {
        marginLeft: "0",
    };

    render() {
        let {width, icon, marginLeft, ...remaining} = this.props;
        let style;

        style = {marginLeft: marginLeft};

        return <Button {...remaining} variant="outline-dark" className="tag-button-wrap"
                       tabIndex={-1} style={style}>
            <Octicon icon={icon} width={width} />
        </Button>;
    }
}