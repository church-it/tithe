// @flow

import React, {Component} from "react";

export type RadioButtonProps = {children?:any, style?:any};
export class RadioButton extends Component<RadioButtonProps> {
    static defaultProps = {
        style: {}
    };
    
    static style = {
        display: "flex",
        height: "min-content",
    };

    render() {
        let {style, children, ...props} = this.props;
        return <div style={RadioButton.style}>
            {children}
            <input {...props} style={{marginLeft: "4px", ...style}} type="radio"/>
        </div>;
    }
}