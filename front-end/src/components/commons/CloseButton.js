// @flow

import React from "react";
import "../styles/CloseButton.css"

export class CloseButton extends React.Component< { onClose: () => any, hidden:boolean, className:string } > {
    static defaultProps = {
        hidden: false,
        className: "",
    };

    render() {
        let {hidden, className} = this.props;

        return <div className={className}>
            <button style={hidden ? {visibility: "hidden"} : {}} onClick={this.props.onClose}
                    className="close-button-wrap close" type="button" tabIndex={-1}>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>;
    }
}