// @flow

export function booleanToNumber(_value:boolean):number {
    return _value ? 1 : 0;
}