// @flow

import React from "react";

export const CONFIRM_ON_CLOSE_TEXT:string = "Se fechares antes de salvar, todas as alterações serão perdidas!\n" +
    "Tens certeza que gostaria de fechar assim mesmo?"; // pt_BR

export const MONEY_LABEL:string = "Dinheiro"; // pt_BR
export const CHEQUE_LABEL:string = "Cheque"; // pt_BR
export const RECEIPT_LABEL:string = "Recibo"; // pt_BR

export function addUnderlineAt(text:string, character:string) {
    let index = text.indexOf(character.toUpperCase());
    if (index < 0) {
        index = text.indexOf(character.toLowerCase());
    }

    let before = text.substring(0,index);
    let underlined = text.charAt(index);
    let after = text.substring(index+1);

    return <>{before}<u>{underlined}</u>{after}</>
}