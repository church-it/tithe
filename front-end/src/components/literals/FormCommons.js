export const INSERT_LABEL:string = "Inserir"; // pt_BR
export const SAVE_LABEL:string = "Salvar"; // pt_BR
export const DELETE_LABEL = "Apagar"; // pt_BR
export const CANCEL_LABEL = "Cancelar"; // pt_BR
export const OK_LABEL = "Fechar"; // pt_BR
export const FINISH_LABEL = "Finializar"; // pt_BR