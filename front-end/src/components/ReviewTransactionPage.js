// @flow
import React from 'react';
import "./styles/ReviewToSignPage.css"
import StateTitleContainer from "../containers/items/StateTitleContainer";
import {ProceedButton} from "./items/ProceedButton";
import {OfferEnvelopeDto} from "../transactions/data/OfferEnvelopeDto";
import {DetailedSummaryCard} from "./items/DetailedSummaryCard";
import {amountToCurrency, CURRENCY_SIGN} from "./commons/LocaleUtils";
import {TITHE_LABEL, TITHES_LABEL} from "./items/input-form/TitheGroup";
import {OFFER_LABEL} from "./items/OfferListDiv";
import {CHEQUE_LABEL, RECEIPT_LABEL} from "./literals/commons";
import {InputTypeEnum, OfferVo} from "../transactions/data/OfferVo";

const proceed_label = "Seguir para: Assinaturas"; // pt_BR
const review_title_label = "Revisão de Itens"; // pt_BR
const destination_label = "Destinos"; // pt_BR
const total_label = "Total"; // pt_BR
const amount_label = "Valor"; // pt_BR
const envelopes_label = "Envelopes"; // pt_BR


export type Props = {
    envelopes:OfferEnvelopeDto[],
    onConfirm:Function,
    onEditItem:(index:number) => any
};

export default class ReviewTransactionPage extends React.Component<Props> {
    render() {
        let {onConfirm} = this.props;

        return <div className="review-page-wrap">
            <div>
                <StateTitleContainer title={review_title_label}/>
                <table className="review-detailed-listing-table">
                    <tbody>
                    {this.envelopeToCard()}
                    </tbody>
                </table>
                <ProceedButton canProceed={true} label={proceed_label} onProceed={onConfirm}/>
            </div>
            <div className="review-summary-division">
                {this.envelopesToSummary()}
            </div>
        </div>
    }

    envelopesToSummary() {
        let {envelopes} = this.props;

        let envelopeCount = envelopes.length;
        let offeringEnvelopesCount = envelopes.filter(i => i.offers.length > 0).length;
        let titheingEnvelopeCount = envelopes.filter(i => i.titheAmount > 0).length;
        let chequeEnvelopesCount = envelopes.filter(i => i.inputTypeId === InputTypeEnum.CHEQUE).length;
        let receiptEnvelopesCount = envelopes.filter(i => i.inputTypeId === InputTypeEnum.RECEIPT).length;
        let destinationList = this.collectDestinationList(envelopes, titheingEnvelopeCount > 0);

        return <table className="review-summary-table table-striped">
            <thead>
            <tr>
                <td/>
                <td>{amount_label}</td>
                <td>{envelopes_label}</td>
            </tr>
            </thead>
            <tbody>
            <tr className="group-start">
                <td>{TITHE_LABEL}</td>
                <td>{this.amountToCurrencyDiv(620)}</td>
                <td>{titheingEnvelopeCount}</td>
            </tr>
            <tr className="group-end">
                <td>{OFFER_LABEL}</td>
                <td>{this.amountToCurrencyDiv(500)}</td>
                <td>{offeringEnvelopesCount}</td>
            </tr>
            <tr>
                <td>{CHEQUE_LABEL}</td>
                <td>{this.amountToCurrencyDiv(0)}</td>
                <td>{chequeEnvelopesCount}</td>
            </tr>
            <tr className="group-end">
                <td>{RECEIPT_LABEL}</td>
                <td>{this.amountToCurrencyDiv(0)}</td>
                <td>{receiptEnvelopesCount}</td>
            </tr>
            <tr className="group-end">
                <td>{destination_label}</td>
                <td colSpan={2}>
                    <span>{destinationList.join(", ")}</span>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td>{total_label}</td>
                <td>{this.amountToCurrencyDiv(1120)}</td>
                <td>{envelopeCount}</td>
            </tr>
            </tfoot>
        </table>;
    }

    collectDestinationList(envelopes:OfferEnvelopeDto[], addTitheDestinationLabel:boolean) {
        let offerList: OfferVo[] = [];
        envelopes.map(i => i.offers).forEach(i => offerList = [...offerList, ...i]);
        let destinationList = [...new Set(offerList.map(i => i.destination))];
        if (addTitheDestinationLabel) {
            destinationList = [TITHES_LABEL, ...destinationList];
        }
        return destinationList;
    }

    envelopeToCard():any {
        let {envelopes, onEditItem} = this.props;
        let key = 0;
        return envelopes.map((i: OfferEnvelopeDto, index) => {
            return <DetailedSummaryCard envelope={i} onClick={() => onEditItem(index)} key={++key}/>
        });
    }

    amountToCurrencyDiv(amount:number):any {
        return <div className="review-currency-div">
            <div>{CURRENCY_SIGN}</div>
            <div>{amountToCurrency(amount, false)}</div>
        </div>;
    }
}