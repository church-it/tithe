// @flow

import React from 'react';
import "./styles/FinishedTransactionPage.css"
import StateTitleContainer from "../containers/items/StateTitleContainer";
import {
    FetchTransactionForEdit,
    RequestRevenueSummaryReportService,
    RequestTitheReportService
} from "../transactions/ReportApiRequest";
import BipartiteButton from "./commons/BipartiteButton";
import Octicon, {CloudDownload, Pencil} from "@primer/octicons-react";
import Button from "react-bootstrap/Button";

const successfully_posted_label = "Transação enviada com sucesso."; // pt_BR
const tithe_summary_report_title = "Sumário de Dízimos"; // pt_BR
const report_label = "Relatório"; // pt_BR
const overall_summarize_report_title = "Entrada Geral"; // pt_BR
const summarizing_report_label = "Relatório de Sumarização"; // pt_BR
const finished_title = "Finalizado!"; // pt_BR

export type Props = {
    transactionHash: string,
    afterFetchForEdit: (data:any) => any,
}

export class FinishedTransactionPage extends React.Component<Props> {
    constructor(props:Props) {
        super(props);
        this.downloadTitheSummaryReport = this.downloadTitheSummaryReport.bind(this);
        this.downloadRevenueSummary = this.downloadRevenueSummary.bind(this);
        this.downloadForEdition = this.downloadForEdition.bind(this);
    }

    render() {
        return <div className="finished-transaction-wrap">
            <StateTitleContainer title={finished_title} canExit={false} />
            <p>{successfully_posted_label}</p>
            <div className="finished-downloads-wrap">
                <DownloadButton onClick={this.downloadTitheSummaryReport}>
                    <b>{tithe_summary_report_title}</b>
                    <span>{report_label}</span>
                </DownloadButton>
                <DownloadButton onClick={this.downloadRevenueSummary}>
                    <b>{overall_summarize_report_title}</b>
                    <span>{summarizing_report_label}</span>
                </DownloadButton>
                <Button variant="outline-secondary" size="sm" style={{margin:"4px",height:"min-content"}}
                        onClick={this.downloadForEdition}>
                    <Octicon icon={Pencil} size="small"/>
                </Button>
            </div>
        </div>;
    }

    downloadTitheSummaryReport = () => {
        RequestTitheReportService(this.props.transactionHash);
    };

    downloadRevenueSummary = () => {
        RequestRevenueSummaryReportService(this.props.transactionHash);
    };

    downloadForEdition = () => {
        let onRejected = r => alert(r);

        let onFulfilled = this.props.afterFetchForEdit;
        FetchTransactionForEdit(this.props.transactionHash, onFulfilled, onRejected);
    };
}

class DownloadButton extends React.Component<any> {
    render() {
        let {onClick, children} = this.props;
        return <BipartiteButton onClick={onClick} icon={CloudDownload} variant="outline-success" children={children}/>
    }
}