import React from 'react';
import '../styles/App.css';

import NavigationRouter from "./NavigationRouter";
import TransactionWindowContainer from "../../containers";
import {Redirect, Route} from "react-router-dom";

const App = () => {
  return (
    <div className="App">
        <NavigationRouter>
            <Route exact path="/">
                <Redirect to="/input" />
            </Route>
            <Route path="/input" component={() => <TransactionWindowContainer /> }/>
        </NavigationRouter>
    </div>
  );
};

export default App;
