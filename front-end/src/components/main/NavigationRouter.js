import React, {Component} from 'react';

import {BrowserRouter as Router, Route} from "react-router-dom";

class NavigationRouter extends Component<{}> {
    render() {
        let {children} = this.props;

        return (
            <Router>
                <Route render={({location, history}) => (
                    <React.Fragment>
                        <main>
                            {children}
                        </main>
                    </React.Fragment>
                )}/>
            </Router>
        );
    }
}

export default NavigationRouter;