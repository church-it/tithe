// @flow

import React from 'react';
import Helmet from "react-helmet";
import StartTransactionPage from "../containers/StartTransactionContainer";
import type {TransactionProgressStateType} from "../redux-states/TransactionProgressState";
import {TransactionProgressStatesEnum} from "../redux-states/TransactionProgressState";
import {withRouter} from "react-router-dom";
import InputTransactionPage from "./InputTransactionPage";
import SignTransactionPage from "../containers/SignTransactionPageContainer";
import CommittedTransactionPage from "../containers/FinishedTransactionPageContainer";
import "./styles/TransactionRoot.css"
import PushTransactionPage from "../containers/PushTransactionPageContainer";
import StateProgressBar from "../containers/items/state-progress/StateProgressBarContainer";
import ReviewTransactionPageContainer from "../containers/ReviewTransactionPageContainer";

const queryString = require('query-string');

const transaction_label = "Dízimo App: Transações"; // pt_BR

// eslint-disable-next-line no-unused-vars
type RouterProps = {
    location:any,
    history:any,
    match:any
}

export type ForceProgressActions = {
    doOpen: () => any,
    doReview: () => any,
    doSignatories: () => any,
    doPush: () => any,
    doFinished: () => any,
}

class TransactionStateSwitch extends React.Component<any> {
    componentDidMount() {
        let {doOpen, doReview, doSignatories, doPush, doFinished, transactionProgressState} = this.props;

        let getParams = queryString.parse(this.props.location.search);

        if (getParams.open !== undefined) {
            doOpen();
        } else if (transactionProgressState === TransactionProgressStatesEnum.CLOSED) {
            if (getParams.review !== undefined) {
                doReview();
            } else if (getParams.signatories !== undefined) {
                doSignatories();
            } else if (getParams.push !== undefined) {
                doPush();
            } else if (getParams.finished !== undefined) {
                doFinished();
            }
        }
    }

    render() {
        let view:any;
        let {transactionProgressState} = this.props;

        view = this.switchViewOnState(transactionProgressState);

        return (
            <div>
                <Helmet>
                    <title>{transaction_label}</title>
                </Helmet>
                <StateProgressBar />
                <div className="transaction-root-wrap">
                    {view}
                </div>
            </div>
        );
    }

    switchViewOnState(transactionProgressState: TransactionProgressStateType):any {
        switch (transactionProgressState) {
            case TransactionProgressStatesEnum.CLOSED:
                return <StartTransactionPage/>;
            case TransactionProgressStatesEnum.OPEN:
                return <InputTransactionPage />;
            case TransactionProgressStatesEnum.REVIEW:
                return <ReviewTransactionPageContainer />;
            case TransactionProgressStatesEnum.SIGN:
                return <SignTransactionPage />;
            case TransactionProgressStatesEnum.WAITING_PUSH:
                return <PushTransactionPage />;
            case TransactionProgressStatesEnum.FINISHED:
                return <CommittedTransactionPage />;
            default:
                return <h1>404 ERROR: {transactionProgressState}</h1>;

        }
    }
}

const TransactionView = withRouter(TransactionStateSwitch);
export default TransactionView;
