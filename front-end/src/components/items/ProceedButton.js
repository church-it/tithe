// @flow
import Button from "react-bootstrap/Button";
import React from "react";
import "../styles/ProceedButton.css"

export type ProceedButtonProps = {
    canProceed: boolean,
    onProceed: () => any,
    label:?any,
    variant?:string
};
export class ProceedButton extends React.Component<ProceedButtonProps> {
    static defaultProps = {
        variant: "outline-secondary",
        children: <div/>,
    };

    buttonRef:any = React.createRef();

    constructor(props:ProceedButtonProps) {
        super(props);
        this.click = this.click.bind(this);
    }

    render() {
        let {canProceed, onProceed, label, variant} = this.props;

        return <div className="proceed-button-wrap">
            <Button disabled={!canProceed} variant={variant} onClick={onProceed} ref={this.buttonRef}>{label}</Button>
        </div>
    }

    click = () => {
        this.buttonRef.current.click();
    }
}