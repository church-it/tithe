// @flow
import React from "react";
import {ProceedButton} from "./ProceedButton";
import "../styles/TransactionSummary.css"
import {OfferEnvelopeDto} from "../../transactions/data/OfferEnvelopeDto";
import {SimpleSummaryCard} from "./SimpleSummaryCard";

export type Props = {
    envelopes:OfferEnvelopeDto[],
    proceedLabel:Element,
    onProceed:() => any,
    canProceed:boolean,
    onSelect: (index:number) => any,
};

export class TransactionSummary extends React.Component<Props> {
    buttonRef:any = React.createRef();

    constructor(props:Props) {
        super(props);
        this.envelopeToSummaryCard = this.envelopeToSummaryCard.bind(this);
    }

    render() {
        let {proceedLabel, canProceed, onProceed} = this.props;

        return <div className="transaction-status-wrap">
            <div className="summary-table-wrap">
                <table>
                    <tbody>
                    {this.envelopeToSummaryCard()}
                    </tbody>
                </table>
            </div>
            <ProceedButton canProceed={canProceed} onProceed={onProceed} label={proceedLabel}
                           ref={this.buttonRef}/>
        </div>;
    }

    envelopeToSummaryCard = ():any => {
        let {envelopes} = this.props;
        const reverse = envelopes.reverse();

        return reverse.map(
            (i: OfferEnvelopeDto, index) => {
                const reverseIndex = envelopes.length - 1 - index;

                return <SimpleSummaryCard key={reverseIndex} fullName={i.fullName}
                                          totalAmount={i.titheAmount + i.offers.map(j => j.amount)
                                              .reduce((total, num) => total + num, 0)}
                                          onClick={() => this.props.onSelect(reverseIndex)}
                />
            });
    };
}
