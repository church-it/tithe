// @flow
import React from "react";
import {PrependedInput} from "../commons/PrependedInput";
import "../styles/OfferValueInput.css";
import "../styles/commons.css"
import {CURRENCY_SIGN} from "../commons/LocaleUtils";

export type Props = {
    amount: number,
    label: string,
    onChange: (amount:number) => any,
    children?:any,
}
export default class OfferValueInput extends React.Component<Props> {

    inputRef:any = React.createRef();

    constructor(props:Props) {
        super(props);
        this.keyPressed = this.keyPressed.bind(this);
        this.focus = this.focus.bind(this);
    }

    render() {
        let {label, amount, children} = this.props;

        return <div className="offer-value-input-wrap">
            <b className="inline-group-title-element">{label}:</b>
            <PrependedInput value={amount.toFixed(2)} title={CURRENCY_SIGN} placeholder="0.00"
                            onChange={nothing} onKeyDown={this.keyPressed} prependWidth={30}
                            ref={this.inputRef}/>
            {children}
        </div>;
    }

    keyPressed = (event: KeyboardEvent) => {
        let amountCents = Math.round(this.props.amount * 100);
        let keyValue = parseInt(event.key);

        if (event.key === "Backspace") {
            amountCents = Math.floor(amountCents / 10);
        } else if (Number.isInteger(keyValue)) {
            amountCents = amountCents * 10 + keyValue;
        } else {
            return;
        }

        event.preventDefault();
        this.props.onChange(amountCents / 100);
    };

    focus = () => {
        this.inputRef.current.focus();
    };
}

function nothing() {
    return () => {
    };
}