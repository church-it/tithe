import React from "react";
import {PrependedInput} from "../../commons/PrependedInput";
import {CloseButton} from "../../commons/CloseButton";
import {CpfMask} from "../../commons/LocaleUtils";
import type {SignatoryVo} from "../../../transactions/data/SignatoryVo";

const name_label = "Nome"; // pt_BR
const person_id_label = "CPF"; // pt_BR
const ID_MASK = CpfMask; // pt_BR

type Props = {
    fieldId:number,
    signatory:SignatoryVo,
    canRemove:boolean,
    onRemove:(id:number)=>any,
    onChange:(id:number,signatory:SignatoryVo)=>any,
    invalidID:boolean,
};

export default class SignatoryField extends React.Component<Props> {
    static invalidInputStyle = {color:"red"};

    constructor(props: Props) {
        super(props);
        this.maskedChange = this.maskedChange.bind(this);
    }

    render() {
        let {signatory, canRemove, onRemove, fieldId, onChange, invalidID} = this.props;

        return <div className="single-signatory-wrap">
            <div>
                <PrependedInput value={signatory.fullName} title={name_label} prependWidth={60}
                                onChange={(e: any) => onChange(fieldId, {fullName: e.target.value, id: signatory.id})}/>
                <PrependedInput value={signatory.id} title={person_id_label} prependWidth={60} maxLength={24}
                                onChange={(e: any) => this.maskedChange(e.target.value)}
                                style={invalidID ? SignatoryField.invalidInputStyle : {}} />
            </div>
            <CloseButton onClose={() => onRemove(fieldId)} hidden={!canRemove}/>
        </div>;
    }

    maskedChange = (value) => {
        let {fieldId, signatory, onChange} = this.props;
        value = ID_MASK(value);

        onChange(fieldId, {id: value, fullName: signatory.fullName});
    };
}