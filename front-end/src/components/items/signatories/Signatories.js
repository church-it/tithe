import React, {Component} from "react";
import type {SignatoryVo} from "../../../transactions/data/SignatoryVo";
import Button from "react-bootstrap/Button";
import SignatoryField from "./SignatoryField";

const add_signatory_label = "+ Adc. Signatário"; // pt_BR

type Props = {
    signatories:SignatoryVo[],
    idsValidity:boolean[],
    onAddSignatory:()=>any,
    onRemoveSignatory:(key:number)=>any,
    onUpdateSignatory:(key:number, signatory:SignatoryVo)=>any,
}

export class Signatories extends Component<Props> {
    render(): any {
        let {signatories, onAddSignatory, onRemoveSignatory, onUpdateSignatory, idsValidity} = this.props;
        let signatoryFields: any[];
        let i = 0;

        signatoryFields = signatories.map((signatory: SignatoryVo) => {
            let fieldId = i++;
            return <SignatoryField fieldId={fieldId} signatory={signatory} canRemove={i > 2} key={fieldId}
                                   onRemove={onRemoveSignatory} onChange={onUpdateSignatory}
                                   invalidID={!idsValidity[fieldId]}/>
        });

        return <div className="signatories-wrap">
            {signatoryFields}
            <Button variant="outline-secondary" size="sm" onClick={onAddSignatory}
                    style={{margin: "4px"}}>
                {add_signatory_label}
            </Button>
        </div>
    }
}