// @flow
import React from "react";
import "../styles/TransactionStepTitle.css";

export default class StateTitle extends React.Component<{title:string}> {

    render() {
        let {title} = this.props;

        return <h3 className="step-title-wrap">
            <span>{title}</span>
        </h3>;
    }
}