// @flow

import React, {Component} from "react";
import "../styles/SimpleSummaryCard.css"
import {amountToCurrency} from "../commons/LocaleUtils";
import {ANONYMOUS_STRING_CODE} from "../../transactions/ReportApiRequest";

type Props = { fullName: string, totalAmount: number, onClick:Function };

export class SimpleSummaryCard extends Component<Props> {
    render() {
        let {totalAmount, fullName, ...remaining} = this.props;
        if (fullName.length === 0) {
            fullName = ANONYMOUS_STRING_CODE;
        }

        return <tr className="simple-summary-card-wrap" {...remaining}>
            <td>{fullName}</td>
            <td>{amountToCurrency(totalAmount)}</td>
        </tr>;
    }
}