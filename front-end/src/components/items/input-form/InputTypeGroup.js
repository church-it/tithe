// @flow
import React, {Component} from "react";
import type {OfferInputType} from "../../../transactions/data/OfferVo";
import {InputTypeEnum} from "../../../transactions/data/OfferVo";
import type {AddKeyBoardHookType} from "../../InputTransactionPage";
import {RadioButton} from "../../commons/RadioButton";
import {addUnderlineAt, CHEQUE_LABEL, MONEY_LABEL, RECEIPT_LABEL} from "../../literals/commons";
import "../../styles/InputTypeGroup.css";

const cheque_number_label = "#cheque no."; // pt_BR
const deposit_type_label = "Tipo da Entrada"; // pt_BR
const money_shortcut = "d"; // pt_BR
const cheque_shortcut = "c"; // pt_BR
const receipt_shortcut = "r"; // pt_BR

export type InputTypeGroupProps = {
    onChangeType: (inputTypeId:OfferInputType) => any,
    onChangeChequeNumber: (chequeNumber:string) => any,
    inputTypeId: OfferInputType,
    chequeTypeNumber:?string,
    addKeyBoardHook: AddKeyBoardHookType,
}
export class InputTypeGroup extends Component<InputTypeGroupProps, {focusCheque:boolean}> {
    state = {
        focusCheque: false,
    };

    chequeInputRef:any = React.createRef();

    constructor(props:InputTypeGroupProps) {
        super(props);
        this.keyBoardEventHandler = this.keyBoardEventHandler.bind(this);
        this.props.addKeyBoardHook(this.keyBoardEventHandler);
        this.onChangeChequeType = this.onChangeChequeType.bind(this);
    }

    render() {
        let {inputTypeId, chequeTypeNumber, onChangeChequeNumber, onChangeType} = this.props;
        let moneySelected = inputTypeId === InputTypeEnum.MONEY;
        let chequeSelected = inputTypeId === InputTypeEnum.CHEQUE;
        let receiptSelected = inputTypeId === InputTypeEnum.RECEIPT;

        chequeTypeNumber = chequeTypeNumber ? chequeTypeNumber : "";

        return <div style={{marginBottom: "8px"}} tabIndex={-1}>
            <b>{deposit_type_label + ":"}</b>
            <div className="input-type-group-wrap">
                <RadioButton onChange={() => onChangeType(InputTypeEnum.MONEY)} checked={moneySelected}
                             tabIndex={-1} radioGroup="inputType">
                    {addUnderlineAt(MONEY_LABEL, money_shortcut)}
                </RadioButton>

                <div>
                    <RadioButton onChange={this.onChangeChequeType} checked={chequeSelected}
                                 tabIndex={-1} radioGroup="inputType">
                        {addUnderlineAt(CHEQUE_LABEL, cheque_shortcut)}
                    </RadioButton>
                    <input placeholder={cheque_number_label} className="cheque-control" value={chequeTypeNumber}
                           disabled={!chequeSelected} onChange={(e) => onChangeChequeNumber(e.target.value)}
                           ref={this.chequeInputRef}/>
                </div>

                <RadioButton onChange={() => onChangeType(InputTypeEnum.RECEIPT)} checked={receiptSelected}
                             tabIndex={-1} radioGroup="inputType">
                    {addUnderlineAt(RECEIPT_LABEL, receipt_shortcut)}
                </RadioButton>
            </div>
        </div>;
    }

    onChangeChequeType = () => {
        this.props.onChangeType(InputTypeEnum.CHEQUE);
        this.setState({focusCheque:true}, () => {
            this.chequeInputRef.current.focus();
            this.setState({focusCheque:true});
        })
    };

    keyBoardEventHandler = (event: KeyboardEvent) => {
        if (event.altKey && event.ctrlKey){
            switch (event.key.toLocaleLowerCase()) {
                case money_shortcut:
                    this.props.onChangeType(InputTypeEnum.MONEY);
                    break;
                case cheque_shortcut:
                    this.onChangeChequeType();
                    break;
                case receipt_shortcut:
                    this.props.onChangeType(InputTypeEnum.RECEIPT);
                    break;
                default:
                    break;
            }
        }
    };
}

