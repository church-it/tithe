// @flow
import React, {Component} from "react";
import Modal from "react-bootstrap/Modal";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import {CANCEL_LABEL, SAVE_LABEL} from "../../literals/FormCommons";

const comment_modal_title = "Comentário sobre Entrada"; // pt_BR

type Props = {
    onHide: () => any,
    onSetComment: (comment:string) => any,
    value:string,
    show:boolean,
    placeholder:string,
};
export class AddCommentModal extends Component<Props> {
    commentControlRef:any = React.createRef();
    
    constructor(props:Props) {
        super(props);

        this.onComment = this.onComment.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.setCurrentComment = this.setCurrentComment.bind(this);
    }

    render() {
        const preventDefault = (event: KeyboardEvent) => event.stopPropagation();
        let {show, placeholder} = this.props;

        return (
            <div onKeyUp={preventDefault} onKeyPress={preventDefault} onKeyDown={preventDefault}>
                <Modal show={show} animation={false} centered onShow={this.setCurrentComment}>
                    <Modal.Header>
                        <Modal.Title>{comment_modal_title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormControl placeholder={placeholder} ref={this.commentControlRef} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.onCancel} variant="secondary">{CANCEL_LABEL}</Button>
                        <Button onClick={this.onComment} variant="primary">{SAVE_LABEL}</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }

    onCancel = () => {
        this.clearAndClose();
    };

    onComment = () => {
        const comment:string = this.getComment();
        this.props.onSetComment(comment);
        this.clearAndClose();
    };

    clearAndClose() {
        this.setComment("");
        this.props.onHide();
    }

    setComment(comment:string) {
        this.commentControlRef.current.value = comment;
    }

    getComment():string {
        return this.commentControlRef.current.value;
    }

    setCurrentComment = () => {
        this.setComment(this.props.value);
    }
}