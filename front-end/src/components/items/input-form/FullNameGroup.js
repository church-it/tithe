import React from "react";

const full_name_tooltip = "Nome completo ou anônimo (vazio)";

type Props = { fullName: string, autoFocus: boolean, onChange: (name: string) => any, buttonRef:any };
export class FullNameGroup extends React.Component<Props> {
    render() {
        let {onChange, fullName, buttonRef, ...props} = this.props;
        return <div style={{paddingBottom: "16px"}}>
            <input {...props} ref={buttonRef} onChange={(e) => onChange(e.target.value)}
                   placeholder={full_name_tooltip} value={fullName} style={{width:"352px"}} />
        </div>;
    }
}