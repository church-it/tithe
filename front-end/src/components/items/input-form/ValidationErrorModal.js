// @flow
import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {OK_LABEL} from "../../literals/FormCommons";
import "../../styles/ValidationErrorModal.css";

const validation_modal_title = "Dados inválidos.";

type OwnState = { messages: string[], tip:string, show: boolean };
export default class ValidationErrorModal extends React.Component<{messages?:string[], tip?:string}, OwnState> {
    state = {
        messages: [],
        tip: "",
        show: false,
    };

    constructor(props:any) {
        super(props);
        let {messages, tip} = this.props;

        if (messages) {
            this.state.messages = messages;
        }
        if (tip) {
            this.state.tip = tip;
        }
    }

    render() {
        const preventDefault = (event: KeyboardEvent) => event.stopPropagation();
        let {messages, tip, show} = this.state;
        let tipDisplay = tip.length === 0 ? {display:"none"} : {};

        return (
            <div onKeyUp={preventDefault} onKeyPress={preventDefault} onKeyDown={preventDefault}>
                <Modal show={show}>
                    <Modal.Header className={"validation-error-header"}>
                        <Modal.Title>{validation_modal_title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ErrorList messages={messages}/>
                        <p style={tipDisplay} className="validation-error-tip">{tip}</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.hide.bind(this)} variant="secondary">{OK_LABEL}</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }

    hide() {
        this.setState({show:false});
    }

    show() {
        this.setState({show:true});
    }

    setMessages(messages:string[]) {
        this.setState({messages})
    }
}

function ErrorList(props:{messages:string[]}) {
    return <ul className={"validation-error-list"}>{
        props.messages.map(value => <li>{value}</li>)
    }</ul>
}