// @flow
import React, {Component} from "react";
import type {AddKeyBoardHookType} from "../../InputTransactionPage";
import Button from "react-bootstrap/Button";
import "../../styles/EnvelopeSubmitGroup.css";
import {DELETE_LABEL, INSERT_LABEL, SAVE_LABEL} from "../../literals/FormCommons";

export const NEW_VARIATION:number = 0;
export const UPDATE_VARIATION:number = 1;

export class EnvelopeSubmitGroup extends Component<{
    variation:number,
    onSubmitOffer: () => any,
    onDeleteOffer: () => any,
    addKeyBoardHook: AddKeyBoardHookType
}> {

    constructor(props: any) {
        super(props);
        this.handleKeyboardEvent = this.handleKeyboardEvent.bind(this);
        this.props.addKeyBoardHook(this.handleKeyboardEvent)
    }

    buttonRef: any = React.createRef();

    render() {
        let text:string = this.props.variation === NEW_VARIATION ? INSERT_LABEL : SAVE_LABEL;
        let hidden:boolean = this.props.variation === NEW_VARIATION;

        return <div className="main-transaction-submit-btn">
            <Button type="button" className="submit-button" variant="primary" ref={this.buttonRef}
                    onClick={this.props.onSubmitOffer} onKeyDown={this.onKeyDown}>{text}</Button>
            <Button type="button" className="submit-button" hidden={hidden} variant="danger"
                    onClick={this.props.onDeleteOffer}>{DELETE_LABEL}</Button>
        </div>;
    }

    onKeyDown = (event: KeyboardEvent) => {
        if (event.ctrlKey && event.altKey) {
            event.preventDefault();
        }
    };

    handleKeyboardEvent = (event: KeyboardEvent) => {
        if (event.ctrlKey && !event.altKey && event.key === "Enter") {
            this.buttonRef.current.click();
        }
    };
}