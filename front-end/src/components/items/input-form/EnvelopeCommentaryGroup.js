// @flow

import React from "react";
import {TagButton} from "../../commons/TagButton";
import {Pencil} from "@primer/octicons-react";
import {AddCommentModal} from "./AddCommentModal";
import "../../styles/EnvelopeCommentaryGroup.css";

const add_comment_label = "Adicionar comentário."; // pt_BR
const comment_modal_placeholder = "Exemplo: \"Inclui dízimo do mês passado.\" (ou vazio)"; // pt_BR
const open_quote_mark = "“"; // pt_BR
const close_quote_mark = "”"; // pt_BR
const comments_label = "Comentários"; // pt_BR

type Props = {
    comment:string,
    onComment: (comment:string) => any,
};

export default class EnvelopeCommentaryGroup extends React.Component<Props, {modalShow:boolean}>{
    state = {
        modalShow: false,
    };

    render() {
        let {modalShow} = this.state;
        let {comment, onComment} = this.props;

        const emptyComment = !comment || comment.length === 0;
        const nonNullComment = emptyComment
            ? comment_modal_placeholder
            : open_quote_mark + comment + close_quote_mark;

        return <div tabIndex={-1} >
            <div className="header_wrapper">
                <b>{comments_label}:</b>
                <TagButton icon={Pencil} width={15} title={add_comment_label}
                           marginLeft="8px" onClick={() => this.setState({modalShow:true})}/>
            </div>

            <p className={emptyComment ? "empty-comment-p" : ""}>
                <i>{nonNullComment}</i>
            </p>

            <AddCommentModal show={modalShow} onHide={() => this.setState({modalShow:false})}
                             onSetComment={onComment} value={comment} placeholder={comment_modal_placeholder} />
        </div>;
    }
}