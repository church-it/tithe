// @flow

import React from 'react';
import '../../styles/InputEnvelopeForm.css';
import {InputTypeEnum} from "../../../transactions/data/OfferVo";
import {DEFAULT_NEW_OFFER, OfferListDiv} from "./../OfferListDiv";
import {NullOrUndefinedUnexpectedError} from "../../../redux-states/errors/NullOrUndefinedUnexpectedError";
import type {AddKeyBoardHookType} from "../../InputTransactionPage";
import {InputTypeGroup} from "./InputTypeGroup";
import {FullNameGroup} from "./FullNameGroup";
import {Header} from "./Header";
import {EnvelopeSubmitGroup, NEW_VARIATION, UPDATE_VARIATION} from "./EnvelopeSubmitGroup";
import {TitheGroup} from "./TitheGroup";
import {booleanToNumber} from "../../commons/TypeUtils";
import {OfferEnvelopeDto} from "../../../transactions/data/OfferEnvelopeDto";
import {CONFIRM_ON_CLOSE_TEXT} from "../../literals/commons";
import EnvelopeCommentaryGroup from "./EnvelopeCommentaryGroup";
import ValidationErrorModal from "./ValidationErrorModal";

type Props = {
    currentRecordId: ?number;
    recordData: ?OfferEnvelopeDto,
    onInsertOffer: (state:OfferEnvelopeDto) => any,
    onRemoveOffer: (index:number) => any,
    onUpdateOffer: (index:number, state:OfferEnvelopeDto) => any,
    onCloseUpdateMode: () => any,
    addKeyBoardHook: AddKeyBoardHookType,
};

const header_new_tag_tooltip = "Esta entrada ainda não foi inserida."; // pt_BR
const header_update_tag_tooltip = "Você está editando um registro já existente."; // pt_BR
const required_cheque_number_message = "O Número do cheque é obrigatório."; // pt_BR
const required_offer_destination_message = "O Destino da oferta é obrigatório."; // pt_BR
const missing_destination_count_message = (count) => { // pt_BR
    let numberConjugate = count === 1 ? '' : 's';
    return `Há ${count} "destino${numberConjugate}" pendete${numberConjugate}.`;
};

type State = { id:?number, envelopeDto: OfferEnvelopeDto, isNew:boolean, forceFocus: boolean };

export class InputEnvelopeForm extends React.Component<Props, State> {

    static emptyRecord:OfferEnvelopeDto = new OfferEnvelopeDto({
        fullName: "",
        comment: "",
        inputTypeId: InputTypeEnum.MONEY,
        chequeNumber: null,
        titheAmount: 0,
        offers: []
    });

    firstFieldRef:any = React.createRef();
    validationModalRef:any = React.createRef();

    constructor(props:Props) {
        super(props);
        this.onSubmitOffer = this.onSubmitOffer.bind(this);
        this.onDeleteOffer = this.onDeleteOffer.bind(this);
        this.render = this.render.bind(this);
        this.confirmThenCloseMode = this.confirmThenCloseMode.bind(this);
        this.setStateOfferEnvelopeDto = this.setStateOfferEnvelopeDto.bind(this);
        this.onInsertOffer = this.onInsertOffer.bind(this);
        this.focusFirstField = this.focusFirstField.bind(this);

        let {currentRecordId, recordData} = this.props;

        this.state = {
            envelopeDto: InputEnvelopeForm.selectBaseState(recordData),
            isNew: recordData == null,
            forceFocus: false,
            id: currentRecordId,
        };
    }

    render = () => {
        let {addKeyBoardHook} = this.props;
        let {isNew} = this.state;
        let {fullName, comment, inputTypeId, chequeNumber, titheAmount, offers} = this.state.envelopeDto;
        let variation = isNew ? NEW_VARIATION : UPDATE_VARIATION;
        let tooltip: string = isNew ? header_new_tag_tooltip : header_update_tag_tooltip;

        return (
            <div className="input-envelope-wrap">
                <Header tooltip={tooltip} variation={variation} hidden={isNew}
                        onCloseUpdateMode={this.confirmThenCloseMode}/>

                <div className="input-envelope-form">
                    <FullNameGroup fullName={fullName} autoFocus buttonRef={this.firstFieldRef}
                                   onChange={(fullName) => this.setStateOfferEnvelopeDto({fullName})}/>
                    <EnvelopeCommentaryGroup comment={comment}
                                             onComment={(comment: string) => this.setStateOfferEnvelopeDto({comment})}/>
                    <InputTypeGroup inputTypeId={inputTypeId} chequeTypeNumber={chequeNumber}
                                    addKeyBoardHook={addKeyBoardHook}
                                    onChangeType={(inputTypeId) => this.setStateOfferEnvelopeDto({inputTypeId})}
                                    onChangeChequeNumber={(chequeNumber) => this.setStateOfferEnvelopeDto({chequeNumber})}/>
                    <TitheGroup titheAmount={titheAmount}
                                onChange={(titheAmount) => this.setStateOfferEnvelopeDto({titheAmount})}
                                onPromote={() => this.promoteTitheToOffer()}/>
                    <OfferListDiv offers={offers} addKeyBoardHook={addKeyBoardHook}
                                  onChange={(offers) => this.setStateOfferEnvelopeDto({offers})}/>
                </div>

                <EnvelopeSubmitGroup variation={variation} addKeyBoardHook={addKeyBoardHook}
                                     onSubmitOffer={this.onSubmitOffer} onDeleteOffer={this.onDeleteOffer}/>
                <ValidationErrorModal ref={this.validationModalRef}/>
            </div>
        );
    };

    componentDidMount(): void {
        if (this.state.forceFocus) {
            this.setState({forceFocus:false}, this.focusFirstField);
        }
    }

    static getDerivedStateFromProps (nextProps: Props, oldState:State):State {

        // either currently on UPDATE and next is NEW
        // or currently NEW and next is UPDATE
        let previouslyNew = oldState.isNew;
        let willBeNew = nextProps.recordData == null;

        // nn prefix standing for `not null`
        let nnOldId = oldState.id ? oldState.id : -1;
        let nnNewId = nextProps.currentRecordId ? nextProps.currentRecordId : -1;

        if ((booleanToNumber(previouslyNew) ^ booleanToNumber(willBeNew))
            || (!previouslyNew && !willBeNew && nnOldId-nnNewId !== 0)) {
            return {
                envelopeDto: InputEnvelopeForm.selectBaseState(nextProps.recordData),
                isNew: willBeNew,
                forceFocus: true,
                id: nextProps.currentRecordId,
            };
        }

        return oldState;
    }

    confirmThenCloseMode = () => {
        if (!OfferEnvelopeDto.match(this.props.recordData, this.state.envelopeDto)) {
            if (!window.confirm(CONFIRM_ON_CLOSE_TEXT)) {
                return;
            }
        }

        this.props.onCloseUpdateMode();
    };

    resetState = (recordData:?OfferEnvelopeDto = this.props.recordData) => {
        this.setStateOfferEnvelopeDto(InputEnvelopeForm.selectBaseState(recordData));
    };

    onSubmitOffer = () => {
        const validateData:string[] = this.validateData();
        if (validateData.length > 0) {
            const errorModal:ValidationErrorModal = this.validationModalRef.current;
            errorModal.setMessages(validateData);
            errorModal.show();
            return;
        }

        if (this.props.recordData != null) {
            this.onSaveOffer();
        } else {
            this.onInsertOffer();
        }

        this.focusFirstField();
    };

    onSaveOffer = () => {
        let currentRecordId:number = NullOrUndefinedUnexpectedError
            .assertOrThrow(this.props.currentRecordId);

        this.props.onUpdateOffer(currentRecordId, this.state.envelopeDto);
    };

    onDeleteOffer = () => {
        let currentRecordId:number = NullOrUndefinedUnexpectedError
            .assertOrThrow(this.props.currentRecordId);

        this.props.onRemoveOffer(currentRecordId);
    };

    static selectBaseState(recordData:?OfferEnvelopeDto = null) {
        if (recordData != null) {
            return recordData;
        } else {
            return InputEnvelopeForm.emptyRecord;
        }
    };

    onInsertOffer = () => {
        this.props.onInsertOffer(this.state.envelopeDto);
        this.resetState();
    };

    setStateOfferEnvelopeDto = (param:any) => {
        this.setState({
            envelopeDto: {
                ...this.state.envelopeDto,
                ...param,
            }
        });
    };

    promoteTitheToOffer = () => {
        let {envelopeDto} = this.state;
        let offers = envelopeDto.offers;

        const newOffer = DEFAULT_NEW_OFFER();
        newOffer.amount = envelopeDto.titheAmount;

        this.setState({
          envelopeDto: new OfferEnvelopeDto({
              ...envelopeDto,
              titheAmount: 0,
              offers: [...offers, newOffer],
          })
        })
    };

    focusFirstField = () => {
        this.firstFieldRef.current.focus();
    };

    validateData() {
        let messages:string[] = [];
        const envelopeDto = this.state.envelopeDto;

        // if input type is CHEQUE, there must be a number.
        const chequeNumber = envelopeDto.chequeNumber;
        if (envelopeDto.inputTypeId === InputTypeEnum.CHEQUE && (!chequeNumber || chequeNumber.length === 0)) {
            messages = [...messages, required_cheque_number_message];
        }

        // every offer must have a destination.
        const countEmpty = envelopeDto.offers
            .filter(value => !value.destination || value.destination.length === 0).length;
        if (countEmpty > 0) {
            messages = [
                ...messages,
                required_offer_destination_message + "\n" + missing_destination_count_message(countEmpty)
            ]
        }

        return messages;
    }
}