// @flow

import React, {Component} from "react";
import OfferValueInput from "../OfferValueInput";
import {ArrowDown} from "@primer/octicons-react";
import "../../styles/TitheGroup.css";
import {TagButton} from "../../commons/TagButton";

export const TITHE_LABEL = "Dízimo"; // pt_BR
export const TITHES_LABEL = "Dízimos"; // pt_BR
const promote_offer_label = "Transformar em nove oferta."; // pt_BR
const advise_tithe_peculiarity_text = <><i>Atenção: <u>dízimo</u> é uma categoria
    exclusiva.<br/>Não a use para <u>ofertas</u>.</i></>; // pt_BR

type Props = {
    titheAmount: number,
    onChange: (amount: number) => any,
    onPromote: () => any,
};
export class TitheGroup extends Component<Props> {
    render() {

        let {titheAmount,onChange,onPromote} = this.props;

        return <div className="tithe-group-wrap">
            <div className="tithe-group-main-line">
                <OfferValueInput amount={titheAmount} label={TITHE_LABEL} onChange={onChange}>
                    <TagButton onClick={onPromote} title={promote_offer_label} icon={ArrowDown} width={10}
                               marginLeft={"4px"}/>
                </OfferValueInput>
            </div>

            <p>{advise_tithe_peculiarity_text}</p>
        </div>;
    }
}

