import React from "react";
import {CloseButton} from "../../commons/CloseButton";
import "../../styles/Header.css";
import {NEW_VARIATION} from "./EnvelopeSubmitGroup";

const income_deposit_title = "Entrada: Dízimo e Ofertas"; // pt_BR
const NEW_VARIATION_LABEL:string = "novo"; // pt_BR
const UPDATE_VARIATION_LABEL:string = "editar"; // pt_BR

type Props = {
    tooltip:string,
    variation:number,
    onCloseUpdateMode: () => any,
    hidden: boolean,
};
export class Header extends React.Component<Props> {
    render() {
        let {tooltip, variation, onCloseUpdateMode, hidden} = this.props;
        const isNew = variation === NEW_VARIATION;
        let tagLabel = isNew ? NEW_VARIATION_LABEL : UPDATE_VARIATION_LABEL
        let className = isNew ? "new-style" : "update-style"

        return <div className="input-form-header">
            <h3>{income_deposit_title}</h3>
            <div className="input-form-header-append">
                <div id="form-tag-card" className={className} title={tooltip}>{tagLabel}</div>
                <CloseButton onClose={onCloseUpdateMode} hidden={hidden}/>
            </div>
        </div>;
    }
}