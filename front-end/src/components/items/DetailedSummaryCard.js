// @flow

import React, {Component} from "react";
import {INPUT_TYPE_NAME, OfferVo} from "../../transactions/data/OfferVo";
import "../styles/DetailedSummaryCard.css"
import {amountToCurrency} from "../commons/LocaleUtils";
import {OfferEnvelopeDto} from "../../transactions/data/OfferEnvelopeDto";
import {ANONYMOUS_STRING_CODE} from "../../transactions/ReportApiRequest";
import {TITHE_LABEL} from "./input-form/TitheGroup";

const input_type_label = "Tipo"; // pt_BR

type Props = {
    envelope:OfferEnvelopeDto,
};

export class DetailedSummaryCard extends Component<Props> {
    render() {
        const {envelope, ...remaining} = this.props;
        let {fullName, titheAmount, inputTypeId, offers} = envelope;

        if (fullName.length === 0) {
            fullName = ANONYMOUS_STRING_CODE;
        }

        return <tr className="detailed-summary-card-wrap" {...remaining}>
            <td className="left-side">
                <b>{fullName}</b><br/>
                <span>{input_type_label + ": " + INPUT_TYPE_NAME[inputTypeId]}</span>
            </td>
            <td className="right-side">
                <table>
                    <tbody>
                    <tr>
                        <td>{TITHE_LABEL}</td>
                        <td>—</td>
                        <td>{amountToCurrency(titheAmount)}</td>
                    </tr>
                    {
                        this.getOfferItem(offers)
                    }
                    </tbody>
                </table>
            </td>
        </tr>;
    }

    getOfferItem(offers:OfferVo[]):any {
        let key = 0;

        return offers.map((i: OfferVo) => <tr key={++key}>
            <td>{i.destination}</td>
            <td>—</td>
            <td>{amountToCurrency(i.amount)}</td>
        </tr>);
    }
}