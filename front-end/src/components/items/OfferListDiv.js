// @flow

import React from "react";
import Button from "react-bootstrap/Button";
import {OfferVo} from "../../transactions/data/OfferVo";
import {CloseButton} from "../commons/CloseButton";
import OfferValueInput from "./OfferValueInput";
import type {AddKeyBoardHookType} from "../InputTransactionPage";
import "../styles/OfferListDiv.css"
import "../styles/commons.css"
import {addUnderlineAt} from "../literals/commons";

const church_label = "Igreja"; // pt_BR
const add_offer_label = "+ Adc. Oferta"; // pt_BR
export const OFFER_LABEL = "Oferta"; // pt_BR
const offer_shortcut = "o"; // pt_BR
const offers_destination_placeholder = "Destino da Oferta"; // pt_BR
const offers_destination_label = "Destino"; // pt_BR

export const DEFAULT_NEW_OFFER = ():OfferVo => new OfferVo({
    amount: 0,
    destination: church_label,
});

export type OfferRecordDivProps = {
    offers: OfferVo[],
    onChange:(offers: OfferVo[]) => any,
    addKeyBoardHook: AddKeyBoardHookType,
}

export class OfferListDiv extends React.Component<OfferRecordDivProps, {focusOffer:boolean}> {
    state = {
        focusOffer:false,
    };

    latestOfferRef:any = React.createRef();

    constructor(props:OfferRecordDivProps) {
        super(props);
        this.onAddOfferOption = this.onAddOfferOption.bind(this);
        this.removeOfferField = this.removeOfferField.bind(this);
        this.createOfferFields = this.createOfferFields.bind(this);
        this.handleKeyBoardEvent = this.handleKeyBoardEvent.bind(this);
        this.props.addKeyBoardHook(this.handleKeyBoardEvent);
    }

    render() {
        return <div>
            <Button variant="light" size="sm" onClick={this.onAddOfferOption} className="offer-add-option">
                {addUnderlineAt(add_offer_label, offer_shortcut)}
            </Button>
            {this.createOfferFields()}
        </div>;
    }

    onAddOfferOption = () => {
        this.props.onChange([
            ...this.props.offers,
            DEFAULT_NEW_OFFER(),
        ]);

        this.setState({focusOffer:true}, () => {
            this.latestOfferRef.current.focus();
            this.setState({focusOffer: false});
        })
    };

    removeOfferField = (key:number) => {
        this.props.onChange([...this.props.offers.slice(0, key), ...this.props.offers.slice(key+1)]);
    };

    createOfferFields = ():any[] => {
        const offerVos = this.props.offers;
        if (offerVos.length === 0) {
            return [];
        }

        let i = 0;
        let offers = offerVos.slice(0, offerVos.length-1).map((offer):any => {
            let key = i++;
            return this.offerFieldFromVo(key, offer);
        });

        let key:number = offers.length;
        let offer:OfferVo = offerVos[key];
        return [...offers, this.offerFieldFromVo(key, offer, this.latestOfferRef)];
    };

    offerFieldFromVo(key:number, offer:OfferVo, ref:any = {}):any {
        return <div className="offer-field-wrap" key={key}>
            <div className="offer-field-header" >
                <OfferValueInput amount={offer.amount} label={OFFER_LABEL} ref={ref}
                                 onChange={(value) => this.changeAmount(key, value)} />
                <CloseButton className="offer-item-close-btn" onClose={() => this.removeOfferField(key)}/>
            </div>
            <div className="offer-destination-wrap">
                <span className="inline-group-title-element">{offers_destination_label}:</span>
                <input type="text" placeholder={offers_destination_placeholder}
                       value={offer.destination}
                       onChange={(e) => this.changeDestination(key, e.target.value)}/>
            </div>
        </div>;
    }

    changeDestination(key:number, value:string) {
        let offer:OfferVo = this.props.offers[key];
        offer.destination = value;
        this.updateOffer(key, offer);
    }

    updateOffer(key:number, offer:OfferVo) {
        this.props.onChange([
            ...this.props.offers.slice(0, key),
            offer,
            ...this.props.offers.slice(key+1)
        ]);
    }

    changeAmount(key: number, value: number) {
        let offer:OfferVo = this.props.offers[key];
        offer.amount = value;
        this.updateOffer(key, offer);
    }

    handleKeyBoardEvent = (event:KeyboardEvent) => {
        if (event.altKey && event.ctrlKey && event.key.toLowerCase() === "o") {
            this.onAddOfferOption();
        }
    }
}

