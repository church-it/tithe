// @flow
import * as React from "react";
import "../../styles/StateProgressBar.css"
import StateProgressItem from "../../../containers/items/state-progress/StateProgressItemContainer";
import {TransactionProgressStatesEnum} from "../../../redux-states/TransactionProgressState";
import CloseTransactionButton from "../../../containers/items/state-progress/CloseTransactionButtonContainer";

export type Props = {progressState:string};
export default class StateProgressBar extends React.Component<Props> {
    render() {
        return <div className="state-progress-wrap">
            <div className="state-progress-bar-wrap">
                <StateProgressItem stateValue={TransactionProgressStatesEnum.OPEN} />
                <StateProgressItem stateValue={TransactionProgressStatesEnum.REVIEW} />
                <StateProgressItem stateValue={TransactionProgressStatesEnum.SIGN} />
                <StateProgressItem stateValue={TransactionProgressStatesEnum.FINISHED} />
            </div>
            <CloseTransactionButton hidden={this.showExitButton()} />
        </div>;
    }

    showExitButton() {
        let {progressState} = this.props;

        switch (progressState) {
            case TransactionProgressStatesEnum.WAITING_PUSH:
            case TransactionProgressStatesEnum.CLOSED:
                return true;
            default:
                return false;
        }
    }
}

