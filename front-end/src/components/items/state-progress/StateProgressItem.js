// @flow
import React from "react";
import Octicon, {ArrowUp, PrimitiveDot, PrimitiveDotStroke} from "@primer/octicons-react";
import Button from "react-bootstrap/Button";
import "../../styles/StateProgressItem.css"

export type Props = {
    active:boolean,
    allowedMove:boolean,
    onClick: (destination:number) => any,
}
export default class StateProgressItem extends React.Component<Props> {
    static size = 25;

    render() {
        let {active, allowedMove, onClick} = this.props;

        if (active) {
            return <div className="state-progress-item-wrap">
                <StateIcon stroke={true}/>
                <Octicon icon={ArrowUp}/>
            </div>;
        } else {
            return <Button variant="outline-dark" className="state-progress-item-wrap"
                           disabled={!allowedMove} onClick={onClick}>
                <StateIcon stroke={false}/>
            </Button>
        }
    }

}

function StateIcon(props:{stroke:boolean}) {
    if (props.stroke) {
        return <Octicon icon={PrimitiveDot} size={StateProgressItem.size}/>;
    } else {
        return <Octicon icon={PrimitiveDotStroke} size={StateProgressItem.size}/>;
    }
}