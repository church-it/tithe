// @flow
import React, {Component} from "react";
import Button from "react-bootstrap/Button";
import Octicon, {SignOut} from "@primer/octicons-react";
import {CONFIRM_ON_CLOSE_TEXT} from "../../literals/commons";
import "../../styles/CloseTransactionButton.css";

type Props = { hidden: boolean, closeTransaction: () => any };
export default class CloseTransactionButton extends Component<Props> {

    constructor(props:Props) {
        super(props);

        this.onCloseTransaction = this.onCloseTransaction.bind(this);
    }

    render() {
        let {hidden} = this.props;
        let hiddenStyleObj = hidden ? {display:"none"} : {};

        return <Button variant="outline-dark" className="exit-button" style={hiddenStyleObj}
                       onClick={this.onCloseTransaction}>
            <Octicon icon={SignOut} size={18}/>
        </Button>;
    }

    onCloseTransaction = () => {
        let {closeTransaction} = this.props;

        if (window.confirm(CONFIRM_ON_CLOSE_TEXT)) {
            closeTransaction();
        }
    }
}