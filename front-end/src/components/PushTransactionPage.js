// @flow

import React from "react";
import ReactLoading from "react-loading";
import "./styles/PushTransactionPage.css"
import type {PreProcessingProps} from "../transactions/ReportApiRequest";
import {sendTransactionData} from "../transactions/ReportApiRequest";
import type {TransactionData} from "../transactions/data/TransactionDataDto";

const error_label = "Erro"; // pt_BR
const pushing_transaction_label = "Enviando transação..."; // pt_BR

const preProcessingProps:PreProcessingProps = {replaceAnonymous:true, offsetDataTimezone:true};

export type PushTransactionPageProps = {
    transactionData: TransactionData,
    onComplete: () => any,
    onError: () => any,
}
export type PushTransactionPageState =  {
    transactionHash: ?string
}
export default class PushTransactionPage extends React.Component<PushTransactionPageProps, PushTransactionPageState> {
    state = {
        transactionHash: null,
    };

    static getDerivedStateFromProps (nextProps:PushTransactionPageProps, prevState:PushTransactionPageState) {
        if (nextProps.transactionData.hashValue !== prevState.transactionHash) {
            sendTransactionData(nextProps.transactionData, preProcessingProps)
                .then(value => PushTransactionPage.checkResultAndAlert(value, nextProps))
                .catch(reason => {
                    alert(error_label + ":\n" + reason);
                    nextProps.onError();
                });

            return {transactionHash: nextProps.transactionData.hashValue}
        }
    }

     static checkResultAndAlert(value:Response, props:PushTransactionPageProps) {
         if (value.ok) {
             props.onComplete()
         } else {
             value.json().then((body) => {
                 alert(error_label + ":\n" + JSON.stringify(body.message, null, 2));
                 props.onError();
             });
         }
     }

    static prettyJson(body:any) {
        return JSON.stringify(body, null, 3);
    }

    render() {
        return <div className="push-transaction-wrap">
            <h3 className="push-transaction-title">{pushing_transaction_label}</h3>
            <ReactLoading type="spokes" color="black"/>
        </div>
    }
}