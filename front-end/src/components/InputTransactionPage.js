// @flow

import React from 'react';
import InputEnvelopeForm from "../containers/items/TransactionInputFormContainer";
import "./styles/InputTransactionPage.css";
import Octicon, {ArrowRight} from "@primer/octicons-react";
import TransactionSummaryContainer from "../containers/items/TransactionSummaryContainer";

export type AddKeyBoardHookType = (handler: (event: KeyboardEvent) => any) => any;

const review_items_label = "Revisar Itens"; // pt_BR

export default class InputTransactionPage extends React.Component<{}> {
    keyboardHooks:Function[] = [];

    static proceedLabel:any = <>{review_items_label} <Octicon icon={ArrowRight}/></>;

    constructor(props:{}) {
        super(props);
        this.addKeyBoardHook = this.addKeyBoardHook.bind(this);
        this.keyBoardEventHandler = this.keyBoardEventHandler.bind(this);
    }

    render() {
        return <div className="input-transaction-page-wrap" onKeyUp={this.keyBoardEventHandler} >
            <InputEnvelopeForm addKeyBoardHook={this.addKeyBoardHook} />
            <TransactionSummaryContainer proceedLabel={InputTransactionPage.proceedLabel} />
        </div>
    }

    keyBoardEventHandler = (event:KeyboardEvent) => {
        // TODO: replace keyCode (deprecated) without losing Mac OS compatibility
        let code = event.keyCode;
        let fromCharCode = String.fromCharCode(code);

        if (code === 13) {
            // Mac OS return key comes undefined, but key code translates to "\r".
            event.key = "Enter";
        } else if (fromCharCode.length === 1) {
            // condition prevents from replacing other keys, like Alt key
            event.key = fromCharCode;
        }

        // let {ctrlKey, altKey, key} = event;
        // console.debug(JSON.stringify({ctrlKey, altKey, code, key}, null, 2));

        this.keyboardHooks.forEach(handler => handler(event))
    };

    addKeyBoardHook = (handler: (event:KeyboardEvent) => any) => {
        this.keyboardHooks = [
            ...this.keyboardHooks,
            handler
        ]
    };
}