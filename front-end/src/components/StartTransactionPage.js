// @flow

import React, {Component} from 'react';
import "./styles/StartTransactionPage.css";
import "./styles/commons.css";
import Button from 'react-bootstrap/Button';
import DatePicker from "./commons/DatePicker";

let startTransactionTitle = "Iniciar Transação: Dízimos e Ofertas"; // pt_BR
let startTransactionButtonTitle = "Iniciar"; // pt_BR
const reference_date_label = "Data Referencial"; // pt_BR

export type Props = {
    isOpen?: Boolean,
    onStart: (date: Date) => any
};
export default class StartTransactionPage extends Component<Props, {date:Date}> {
    constructor(props:Props) {
        super(props);

        let current = new Date();
        current.setUTCHours(current.getHours());

        this.state = {
            date: current
        }
    };

    render() {
        let {date} = this.state;

        return (
            <div className="start-transaction-wrap">
                <h3 className="title">{startTransactionTitle}</h3>

                <p className="date-picker-group">
                    <label className="form-label" >{reference_date_label}:</label>
                    <DatePicker onChange={(date) => this.setState({date})} valueAsDate={date}/>
                </p>

                <Button className="startTransaction-button" size="lg"
                        onClick={() => this.props.onStart(date)}
                        variant="outline-primary">
                    {startTransactionButtonTitle}
                </Button>
            </div>
        );
    }
}