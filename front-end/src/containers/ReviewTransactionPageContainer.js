import {connect} from "react-redux";
import type {Props} from "../components/ReviewTransactionPage";
import ReviewTransactionPage from "../components/ReviewTransactionPage";
import type {State} from "../reducers";
import {backwardStateAction, forwardStateAction} from "../actions/TransactionStateActions";
import {openEditionAction} from "../actions/TransactionInputFormActions";


function stateToProps(state:State) {
    return {
        envelopes: state.transactionData.envelopeDtos,
    }
}

function dispatchToProps(dispatch) {
    return {
        onConfirm: () => dispatch(forwardStateAction()),
        onEditItem: (index) => {
            dispatch(backwardStateAction());
            dispatch(openEditionAction(index));
        },
    }
}

function mergeProps(stateProps, dispatchProps, ownProps):Props {
    return {
        ...stateProps, ...dispatchProps, ...ownProps
    }
}

export default connect(stateToProps, dispatchToProps, mergeProps)(ReviewTransactionPage);