import {connect} from "react-redux";
import type {Props} from "../components/SignTransactionPage";
import {SignTransactionPage} from "../components/SignTransactionPage";
import type {SignatoryVo} from "../transactions/data/SignatoryVo";
import {commitSignatoriesAction} from "../actions/TransactionSignatoriesActions";
import {forwardStateAction} from "../actions/TransactionStateActions";

function stateToProps() {
    return {}
}

function dispatchToProps(dispatch):Props {
    return {
        onCommit: (signatories:SignatoryVo[]) => {
            dispatch(commitSignatoriesAction(signatories));
            dispatch(forwardStateAction());
        }
    }
}

export default connect(stateToProps, dispatchToProps)(SignTransactionPage);