// #flow
import StateTitle from "../../components/items/StateTitle";
import {connect} from "react-redux";
import PropTypes from 'prop-types';

function stateToProps(state, ownProps) {
    return {
        title: ownProps.title
    };
}

const StateTitleContainer = connect(stateToProps)(StateTitle);
StateTitleContainer.propTypes = {
    canExit: PropTypes.bool,
    title: PropTypes.string.isRequired,
};
StateTitleContainer.defaultProps = {
    canExit: true,
};

export default StateTitleContainer;