// @flow

import {connect} from "react-redux";
import {InputEnvelopeForm} from "../../components/items/input-form/InputEnvelopeForm";
import {Dispatch} from "redux";
import {
    addOfferEnvelopeAction,
    removeOfferEnvelopeAction,
    updateOfferEnvelopeAction
} from "../../actions/OfferEnvelopeListActions";
import {closeEditionAction} from "../../actions/TransactionInputFormActions";
import type {State} from "../../reducers";
import {OfferEnvelopeDto} from "../../transactions/data/OfferEnvelopeDto";

const mapStateToProps = (state:State) => {
    let currentRecordId = state.transactionItemView.currentRecordId;
    if (currentRecordId === null || currentRecordId === undefined) {
        return {
            recordData: null,
            currentRecordId: null
        };
    }

    let notNullRecordId: number = currentRecordId;
    return {
        recordData: state.transactionData.envelopeDtos[notNullRecordId],
        currentRecordId: state.transactionItemView.currentRecordId
    }
};

const mapDispatchToProps = (dispatch:Dispatch) => {
    return {
        onInsertOffer: (tithingRecordState:OfferEnvelopeDto) => {
            dispatch(addOfferEnvelopeAction(tithingRecordState));
        },
        onRemoveOffer: (index:number) => {
            dispatch(removeOfferEnvelopeAction(index));
            dispatch(closeEditionAction());
        },
        onUpdateOffer: (index:number, tithingRecordState:OfferEnvelopeDto) => {
            dispatch(updateOfferEnvelopeAction(index, tithingRecordState));
            dispatch(closeEditionAction());
        },
        onCloseUpdateMode: () => {
            dispatch(closeEditionAction());
        }
    };
};

const TransactionInputFormContainer = connect(mapStateToProps, mapDispatchToProps)(InputEnvelopeForm);
export default TransactionInputFormContainer;