// @flow
import {connect} from "react-redux";
import type {Props} from "../../components/items/TransactionSummary";
import {TransactionSummary} from "../../components/items/TransactionSummary";
import type {State} from "../../reducers";
import {forwardStateAction} from "../../actions/TransactionStateActions";
import PropTypes from "prop-types";
import {openEditionAction} from "../../actions/TransactionInputFormActions";

function stateToProps(state: State) {
    const offerEnvelopeDtos = state.transactionData.envelopeDtos;
    return {
        canProceed: offerEnvelopeDtos.length > 0,
        envelopes: [...offerEnvelopeDtos],
    }
}

function dispatchToProps(dispatch) {
    return {
        onProceed: () => dispatch(forwardStateAction()),
        onSelect: (index) => dispatch(openEditionAction(index)),
    }
}

function mergeProps(stateProps, dispatchProps, ownProps):Props {
    return { ...stateProps, ...dispatchProps, ...ownProps };
}

const TransactionSummaryContainer = connect(stateToProps, dispatchToProps, mergeProps)(TransactionSummary);
export default TransactionSummaryContainer;

TransactionSummaryContainer.propTypes = {
    proceedLabel: PropTypes.object.isRequired,
};