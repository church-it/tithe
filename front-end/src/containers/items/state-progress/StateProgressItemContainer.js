// @flow
import StateProgressItem from "../../../components/items/state-progress/StateProgressItem";
import type {State} from "../../../reducers";
import {connect} from "react-redux";
import {TransactionProgressStatesEnum} from "../../../redux-states/TransactionProgressState";
import {backwardStateAction} from "../../../actions/TransactionStateActions";
import PropTypes from 'prop-types';
import type {Props} from "../../../components/items/state-progress/StateProgressItem"
import type {TransactionProgressStateType} from "../../../redux-states/TransactionProgressState";

const OPEN_STATE_ID = 1;
const REVIEW_STATE_ID = 2;
const SIGN_STATE_ID = 3;
const FINISHED_STATE_ID = 4;

type FromTo = { from: number, to: number };
type BackwardsFromTo = {action: (from: number, to: number) => any};


function stateToIndex(state:TransactionProgressStateType):number {
    switch (state) {
        case TransactionProgressStatesEnum.OPEN:
            return OPEN_STATE_ID;
        case TransactionProgressStatesEnum.REVIEW:
            return REVIEW_STATE_ID;
        case TransactionProgressStatesEnum.SIGN:
            return SIGN_STATE_ID;
        case TransactionProgressStatesEnum.FINISHED:
            return FINISHED_STATE_ID;
        default:
            return -1;
    }
}

function allowedInteraction(from:number, to:number):boolean {
    return from !== -1 && to !== -1 && from <= SIGN_STATE_ID && from > to;
}

function stateToProps(state:State, ownProps:{stateValue: TransactionProgressStateType}):FromTo {
    return {
        from: stateToIndex(state.transactionProgressState),
        to: stateToIndex(ownProps.stateValue),
    }
}

function dispatchToProps(dispatch):BackwardsFromTo {
    return {
        action: (from: number, to: number) => {
            for (; from > to && from > 1; --from) {
                dispatch(backwardStateAction());
            }
        }
    };
}

function mergeProps(fromTo:FromTo, dispatchToProps:BackwardsFromTo):Props {
    let {from, to} = fromTo;

    return {
        active: from === to,
        allowedMove: allowedInteraction(from, to),
        onClick: () => dispatchToProps.action(from, to),
    }
}

const StateProgressItemContainer = connect(stateToProps, dispatchToProps, mergeProps)(StateProgressItem);
export default StateProgressItemContainer;

StateProgressItemContainer.propTypes = {
    stateValue: PropTypes.any.isRequired
};
