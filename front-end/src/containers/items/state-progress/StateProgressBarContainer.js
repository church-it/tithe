// @flow
import StateProgressBar from "../../../components/items/state-progress/StateProgressBar";
import type {State} from "../../../reducers";
import type {Props} from "../../../components/items/state-progress/StateProgressBar";
import {connect} from "react-redux";

function stateToProps(state:State) {
    return {
        progressState: state.transactionProgressState
    }
}

export default connect<Props>(stateToProps)(StateProgressBar);