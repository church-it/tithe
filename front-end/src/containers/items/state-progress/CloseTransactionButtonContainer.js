// @flow
import CloseTransactionButton from "../../../components/items/state-progress/CloseTransactionButton";
import {connect} from "react-redux";
import {closeTransactionAction} from "../../../actions/TransactionStateActions";
import PropTypes from "prop-types";

function stateToProps() {return {}}

function dispatchToProps(dispatch) {
    return {
        closeTransaction: () => dispatch(closeTransactionAction()),
    }
}

const CloseTransactionButtonContainer = connect(stateToProps, dispatchToProps)(CloseTransactionButton);
export default CloseTransactionButtonContainer;

CloseTransactionButtonContainer.propTypes = {
    hidden: PropTypes.bool,
};
CloseTransactionButtonContainer.defaultProps = {
    hidden: false,
};