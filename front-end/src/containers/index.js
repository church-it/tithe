// @flow

import {connect} from "react-redux";
import type {ForceProgressActions} from "../components";
import TransactionView from "../components";
import {forwardStateAction, openTransactionAction} from "../actions/TransactionStateActions";
import {addOfferEnvelopeAction} from "../actions/OfferEnvelopeListActions";
import {InputTypeEnum, OfferVo} from "../transactions/data/OfferVo";
import type {State} from "../reducers";
import type {SignatoryVo} from "../transactions/data/SignatoryVo";
import type {TransactionData} from "../transactions/data/TransactionDataDto";
import {OfferEnvelopeDto} from "../transactions/data/OfferEnvelopeDto";

export const FOO_TRANSACTION_DATA = (date:Date):TransactionData => ({
    hashValue: "olar",
    signatories: FOO_SIGNATORIES(),
    envelopeDtos: [FOO_TITHE()],
    date: date,
});
export const FOO_TITHE = (name:?string):OfferEnvelopeDto => new OfferEnvelopeDto({
    fullName: (name ? name : "Eduardo Pacheco"),
    comment: "Referente ao mês passado.",
    titheAmount: 310,
    inputTypeId: InputTypeEnum.MONEY,
    chequeNumber: null,
    offers: [
        new OfferVo({
            amount: 150,
            destination: "Igreja",
        }),
        new OfferVo({
            amount: 100,
            destination: "JMN",
        })
    ]
});
export const FOO_SIGNATORIES = ():SignatoryVo[] => [
    {
        fullName: "Edulardo Chepaco",
        id: "0800 8787 03 87"
    }, {
        fullName: "Thiago Riolino",
        id: "4969 58 39"
    }
];

const mapStateToProps = (state:State) => {
    return {transactionProgressState: state.transactionProgressState};
};

function doSignOffStart(dispatch) {
    doAutoStart(dispatch);
    dispatch(addOfferEnvelopeAction(FOO_TITHE("Juliano Robinson")));
    dispatch(addOfferEnvelopeAction(FOO_TITHE("Antônio Camargo Roler")));
    dispatch(forwardStateAction());
}

function doSignatories(dispatch) {
    doSignOffStart(dispatch);
    dispatch(forwardStateAction());
}

function doAutoStart(dispatch) {
    dispatch(openTransactionAction(new Date()));
}

function doWaitingPush(dispatch) {
    doSignatories(dispatch);
    dispatch(forwardStateAction());
}

function doFinished(dispatch) {
    doWaitingPush(dispatch);
    dispatch(forwardStateAction());
}

const dispatchToProps = (dispatch):ForceProgressActions => {
    return {
        doOpen: () => doAutoStart(dispatch),
        doReview: () => doSignOffStart(dispatch),
        doSignatories: () => doSignatories(dispatch),
        doPush: () => doWaitingPush(dispatch),
        doFinished: () => doFinished(dispatch),
    }
};

export default connect(mapStateToProps, dispatchToProps)(TransactionView);