// @flow
import {connect} from "react-redux";
import type {Props} from "../components/FinishedTransactionPage";
import {FinishedTransactionPage} from "../components/FinishedTransactionPage";
import type {State} from "../reducers";
import {editTransactionAction} from "../actions/TransactionStateActions";
import type {TransactionData} from "../transactions/data/TransactionDataDto";

function stateToProps(state:State) {
    return { transactionHash: state.transactionData.hashValue };
}

function dispatchToProps(dispatch) {
    return {
        afterFetchForEdit: (data:TransactionData) => {
            dispatch(editTransactionAction(data))
        }
    };
}

export default connect<Props>(stateToProps, dispatchToProps)(FinishedTransactionPage);