// @flow

import {connect} from "react-redux";
import StartTransactionPage from "../components/StartTransactionPage";
import {Dispatch} from "redux";
import {openTransactionAction} from "../actions/TransactionStateActions";
import {TransactionProgressStatesEnum} from "../redux-states/TransactionProgressState";
import type {State} from "../reducers";


const mapStateToProps = (state:State) => {
    return {
        isOpen: state.transactionProgressState === TransactionProgressStatesEnum.OPEN
    }
};

const mapDispatchToProp = (dispatch: Dispatch) => {
    return {
        onStart: (date) => (dispatch(openTransactionAction(date)))
    }
};

export default connect(mapStateToProps, mapDispatchToProp)(StartTransactionPage);