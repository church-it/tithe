// @flow

import {connect} from 'react-redux';
import PushTransactionPage from "../components/PushTransactionPage";
import type {State} from "../reducers";
import {backwardStateAction, forwardStateAction} from "../actions/TransactionStateActions";

function stateToProps(state:State) {
    return {transactionData: state.transactionData};
}

function dispatchToProps(dispatch) {
    return {
        onComplete: () => dispatch(forwardStateAction()),
        onError: () => dispatch(backwardStateAction()),
    }
}

export default connect(stateToProps, dispatchToProps)(PushTransactionPage);