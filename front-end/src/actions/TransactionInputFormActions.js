// @flow


export const DetailedViewActionEnum = Object.freeze({
    OPEN: "OPEN_DETAILED_VIEW",
    CLOSE: "CLOSE_DETAILED_VIEW"
});
export type DetailedViewActionTypes = $Values<typeof DetailedViewActionEnum>;
export type DetailedViewAction =
    | { type:DetailedViewActionTypes, id:number }
    | { type:DetailedViewActionTypes };

export const openEditionAction = (index: number):DetailedViewAction => {
    return {
        type: DetailedViewActionEnum.OPEN,
        id: index
    };
};

export const closeEditionAction = ():DetailedViewAction => {
    return {
        type: DetailedViewActionEnum.CLOSE
    };
};