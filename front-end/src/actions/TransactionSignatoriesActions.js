// @flow
import type {SignatoryVo} from "../transactions/data/SignatoryVo";

export const CommitSignatoriesActionEnum = Object.freeze({ COMMIT: "COMMIT_SIGNATORIES_ACTION"});
export type CommitSignatoriesActionTypes = $Values<typeof CommitSignatoriesActionEnum>;

export type TransactionSignatoriesAction = {
    type: CommitSignatoriesActionTypes,
    signatories:SignatoryVo[]
};

export function commitSignatoriesAction(signatories:SignatoryVo[]):TransactionSignatoriesAction {
    return {
        type: CommitSignatoriesActionEnum.COMMIT,
        signatories
    }
}