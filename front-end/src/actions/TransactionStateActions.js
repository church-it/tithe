// @flow

import type {TransactionData} from "../transactions/data/TransactionDataDto";

const TransactionProgressActionEnum = {
    OPEN: "OPEN-TRANSACTION-ACTION",
    EDIT: "EDIT-TRANSACTION-ACTION",
    CLOSE: "CLOSE-TRANSACTION-ACTION",
    FORWARD: "FORWARD_STATE_ACTION",
    BACKWARD: "BACKWARD_STATE_ACTION",
};

export {TransactionProgressActionEnum};

export const openTransactionAction = (date:Date) => {
    return {
        type: TransactionProgressActionEnum.OPEN,
        date
    };
};

export const editTransactionAction = (data:TransactionData) => {
    return {
        type: TransactionProgressActionEnum.EDIT,
        data,
    };
};

export const closeTransactionAction = () => {
    return {
        type: TransactionProgressActionEnum.CLOSE
    };
};

export const forwardStateAction = () => {
    return {
        type: TransactionProgressActionEnum.FORWARD
    };
};

export const backwardStateAction = () => {
    return {
        type: TransactionProgressActionEnum.BACKWARD
    };
};