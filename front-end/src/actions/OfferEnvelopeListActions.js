// @flow

import {OfferEnvelopeDto} from "../transactions/data/OfferEnvelopeDto";

export const OfferEnvelopeListActionsEnum = Object.freeze({
    ADD: "ADD",
    REMOVE: "REMOVE",
    UPDATE: "UPDATE"
});
export type OfferEnvelopeListActionType = $Values<typeof OfferEnvelopeListActionsEnum>;
export type TransactionListAction =
    { type: OfferEnvelopeListActionType, record: OfferEnvelopeDto }
    | { type: OfferEnvelopeListActionType, id: number }
    | { type: OfferEnvelopeListActionType, id: number, record: OfferEnvelopeDto};

export const addOfferEnvelopeAction = (envelopeDto: OfferEnvelopeDto): TransactionListAction => {
    return {
        type: OfferEnvelopeListActionsEnum.ADD,
        record: envelopeDto
    };
};

export const removeOfferEnvelopeAction = (id: number): TransactionListAction => {
    return {
        type: OfferEnvelopeListActionsEnum.REMOVE,
        id
    };
};

export const updateOfferEnvelopeAction = (id: number, envelopeDto:OfferEnvelopeDto): TransactionListAction => {
    return {
        type: OfferEnvelopeListActionsEnum.UPDATE,
        id,
        record: envelopeDto
    };
};