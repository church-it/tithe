// @flow

import type {TransactionData} from "./data/TransactionDataDto";
import {OfferEnvelopeDto} from "./data/OfferEnvelopeDto";

const API_HOST_ADDRESS = 'http://localhost:8080';
const PUSH_TRANSACTION_PATH = "/push";
const PULL_TRANSACTION_PATH = "/pull";
const TITHE_REPORT_SERVICE_PATH = "/tithe-report";
const REVENUE_REPORT_SERVICE_PATH = "/revenue-report";

// const ANONYMOUS_STRING_CODE:string = String.fromCharCode(parseInt('033', 8));
export const ANONYMOUS_STRING_CODE:string = "<anônimo>"; // pt_BR

export function RequestTitheReportService(hashValue:string) {
    let servicePath = API_HOST_ADDRESS + TITHE_REPORT_SERVICE_PATH + "?hashValue=" + hashValue;

    return window.open(servicePath);
}

export function RequestRevenueSummaryReportService(hashValue:string) {
    let servicePath = API_HOST_ADDRESS + REVENUE_REPORT_SERVICE_PATH + "?hashValue=" + hashValue;

    return window.open(servicePath);
}

export function FetchTransactionForEdit(hashValue:string, onFulfilled:(data:any)=>any, onRejected:(reason:any)=>any) {
    let servicePath = API_HOST_ADDRESS + PULL_TRANSACTION_PATH + "?hashValue=" + hashValue;
    let payload = {
        method: 'GET',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
    };

    console.info(payload);

    fetch(servicePath, payload)
        .then(value => value.json(), onRejected)
        .then(value => {onFulfilled(prepareTransactionDataIncome(value))}, onRejected)
        .catch(onRejected);
}

function prepareTransactionDataIncome(value:TransactionData):TransactionData {
    value.envelopeDtos = anonToEmpty(value.envelopeDtos);
    return value;
}

function anonToEmpty(envelopeDtos:OfferEnvelopeDto[]):OfferEnvelopeDto[] {
    return envelopeDtos.map(value => new OfferEnvelopeDto({
        ...value,
        fullName: value.fullName === ANONYMOUS_STRING_CODE ? "" : value.fullName
    }));
}

function emptyToAnon(envelopeDtos:OfferEnvelopeDto[]):OfferEnvelopeDto[] {
    return envelopeDtos.map(value => new OfferEnvelopeDto({
        ...value,
        fullName: value.fullName.length === 0 ? ANONYMOUS_STRING_CODE : value.fullName
    }));
}

export function sendTransactionData(transactionData:TransactionData, preProcessing:PreProcessingProps):Promise<Response> {
    if (preProcessing.replaceAnonymous) {
        transactionData.envelopeDtos = emptyToAnon(transactionData.envelopeDtos);
    }

    if (preProcessing.offsetDataTimezone) {
        const date = transactionData.date;
        if (!date) { throw new Error("empty date @ sendTransactionData"); }
        date.setTime(date.getTime()+date.getTimezoneOffset()*60*1000);
    }

    let servicePath = API_HOST_ADDRESS + PUSH_TRANSACTION_PATH;
    let payload = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify(transactionData)
    };

    console.info(payload);
    return fetch(servicePath, payload);
}

export type PreProcessingProps = {
    replaceAnonymous: boolean,
    offsetDataTimezone: boolean,
}