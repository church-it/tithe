// @flow

import {CHEQUE_LABEL, MONEY_LABEL, RECEIPT_LABEL} from "../../components/literals/commons";

export class OfferVo {
    amount:number;
    destination:string = "";

    constructor(props: {amount: number, destination: string}) {
        let {destination, amount} = props;
        this.amount = amount;
        this.destination = destination;
    }

    static clone(objA:OfferVo):OfferVo {
        return new OfferVo({
            amount: Number(objA.amount),
            destination: String(objA.destination),
        });
    }

    static match(objA:OfferVo, objB:OfferVo):boolean {
        try {
            return objA.amount === objB.amount
                && objA.destination === objB.destination;
        } catch (e) {
            return false;
        }
    }
}

export const INPUT_TYPE_NAME:string[] = Object.freeze([MONEY_LABEL, CHEQUE_LABEL, RECEIPT_LABEL]);
export const InputTypeEnum = Object.freeze({
    MONEY: 0,
    CHEQUE: 1,
    RECEIPT: 2
});
export type OfferInputType = $Values<typeof InputTypeEnum>;