// @flow

import type {OfferInputType} from "./OfferVo";
import {OfferVo} from "./OfferVo";

export class OfferEnvelopeDto {
    fullName: string;
    comment: string;
    titheAmount: number;
    inputTypeId: OfferInputType;
    chequeNumber: ?string;
    offers: Array<OfferVo>;

    constructor(props: { fullName:string, comment:string, titheAmount:number,
        inputTypeId:OfferInputType, chequeNumber: ?string, offers: Array<OfferVo>
    }) {
        let {fullName, comment, titheAmount, inputTypeId, chequeNumber, offers} = props;
        this.fullName = fullName;
        this.titheAmount = titheAmount;
        this.inputTypeId = inputTypeId;
        this.chequeNumber = chequeNumber;
        this.offers = offers;
        this.comment = comment;
    }

    static clone = (objA: OfferEnvelopeDto): OfferEnvelopeDto => {
        return new OfferEnvelopeDto({
            fullName: String(objA.fullName),
            comment: String(objA.comment),
            titheAmount: Number(objA.titheAmount),
            inputTypeId: objA.inputTypeId,
            chequeNumber: objA.chequeNumber === null ? null : String(objA.chequeNumber),
            offers: objA.offers.map<OfferVo>(offers => OfferVo.clone(offers))
        });
    };

    static match(objA: ?OfferEnvelopeDto, objB: ?OfferEnvelopeDto): boolean {
        if (objA === undefined || objB === undefined) {
            return false
        }
        if (objA === null) {
            return objB === null
        }
        if (objB === null) {
            return false
        }

        return objA.fullName === objB.fullName
            && objA.comment === objB.comment
            && objA.inputTypeId === objB.inputTypeId
            && objA.chequeNumber === objB.chequeNumber
            && objA.titheAmount === objB.titheAmount
            && this.matchOfferArray(objA.offers, objB.offers);
    }

    static matchOfferArray(offersA: OfferVo[], offersB: OfferVo[]): boolean {
        try {
            if (offersA.length === offersB.length) {
                for (let i: number = 0; i < offersA.length; ++i) {
                    if (!OfferVo.match(offersA[i], offersB[i])) {
                        return false;
                    }
                }
                return true;
            }
        } catch (e) {
        }

        return false;
    }
}