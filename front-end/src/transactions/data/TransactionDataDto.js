// @flow

import type {SignatoryVo} from "./SignatoryVo";
import {OfferEnvelopeDto} from "./OfferEnvelopeDto";

export type TransactionData = {
    envelopeDtos: OfferEnvelopeDto[],
    signatories: ?SignatoryVo[],
    date: ?Date,
    hashValue: string,
};