// @flow

export type SignatoryVo = {
  fullName: string,
  id: string
};